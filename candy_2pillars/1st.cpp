// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

int T, N;
int t;

#define MAX_T 10
#define MAX_L 100
#define MAX_N 100

int data[MAX_N];
int cache[MAX_T][MAX_N][MAX_L];

#define max(a,b) ((a>b)?(a):(b))
#define abs(a) ((a>=0)?(a):(-(a)))

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d", &N);

        for (int i = 0; i < N; ++i)
            for (int j = 0; j < MAX_L; ++j)
                cache[t][i][j] = -1;

        scanf("%d", &data[0]);
        cache[t][0][0] = 0;
        cache[t][0][data[0]] = data[0];

        for (int n = 1; n < N; ++n) {
            scanf("%d", &data[n]);

            for (int d = 0; d < MAX_L; ++d) {
                if (cache[t][n-1][d] == -1) continue;
                int high = cache[t][n-1][d];
                int low = high - d;
                int dh = high + data[n] - low;
                int dhh = high + data[n];
                int dl = abs(high - (low + data[n]));
                int dlh = max(high, low + data[n]);
                cache[t][n][d] = max(cache[t][n][d], cache[t][n-1][d]);
                cache[t][n][dh] = max(cache[t][n][dh], dhh);
                cache[t][n][dl] = max(cache[t][n][dl], dlh);
            }
        }
        for (int n = 0; n < N; ++n) {
            for (int d = 0; d <= 20; ++d) {
                printf("%d ", cache[t][n][d]);
            }
            printf("\n");
        }
        printf("%d\n", cache[t][N-1][0]);
    }

    return 0;
}
