// PI

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define MAX_N 10001
#define INF 987654321
#define min(a,b) ((a<b)?a:b)

int T, N;
int t;
char data[MAX_N];
int cache[MAX_N];

int classify(int start, int end) {
    bool chk = true;
    for (int i = start; i <= end; i++) {
        if (data[start] != data[i]) {
            chk = false;
            break;
        }
    }
    if (chk) return 1;

    chk = true;
    int dif = data[start+1] - data[start];
    if (dif == 1 || dif == -1) {
        for (int i = start+2; i <= end; i++) {
            if (data[i] - data[i-1] != dif) {
                chk = false;
                break;
            }
        }
        if (chk) return 2;
    }

    chk = true;
    for (int i = start+2; i <= end; i++) {
        if (data[i] != data[i-2]) {
            chk = false;
            break;
        }
    }
    if (chk) return 4;

    chk = true;
    if (dif > 0) {
        for (int i = start+2; i <= end; i++) {
            if (data[i] - data[i] <= 0 ) {
                chk = false;
                break;
            }
        }
    } else {
        for (int i = start+2; i <= end; i++) {
            if (data[i] - data[i] >= 0 ) {
                chk = false;
                break;
            }
        }
    }
    if (chk) return 5;

    return 10;
}

int minLevel(int pos) {
    if (pos == N) return 0;

    int &ret = cache[pos];
    if (ret != -1) return ret;
    ret = INF;
    for (int i = 3; i <= 5; i++) {
        if (pos + i <= N) {
            ret = min(ret, minLevel(pos+i) + classify(pos, pos+i-1));
        }
    }
    return ret;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%s", data);
        N = 0;
        while (data[N] != 0) {
            cache[N] = -1;
            N++;
        }
        printf("%d %d\n", N, minLevel(0));
    }

    return 0;
}
