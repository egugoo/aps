// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define max(a,b) ((a<b)?b:a)

#define MAX_N 500

int T, N;
int t;
int Data[MAX_N][MAX_N];
int Cache[MAX_N][MAX_N];

int dir[4][2] = {{-1,0}, {1,0}, {0, -1}, {0, 1}};

int days(int y, int x) {
    if (y < 0 || x < 0 || N <= y || N <= x) return -1;

    int &ret = Cache[y][x];
    if (ret != -1) return ret;

    ret = 1;
    for (int i = 0; i < 4; i++) {
        int ny = y+dir[i][0];
        int nx = x+dir[i][1];
        if (0 <= ny && 0 <= nx && ny < N && nx < N &&
            Data[y][x] < Data[ny][nx]) {
            ret = max(ret, 1 + days(ny, nx));
        }
    }

    return ret;
}

int main()
{
    // freopen("input.txt", "rt", stdin);

    // scanf("%d", &T);
    T = 1;

    for (t = 0; t < T; t++) {
        scanf("%d", &N);

        for (int y = 0; y < N; y++)
            for (int x = 0; x < N; x++) {
                scanf("%d", &Data[y][x]);
                Cache[y][x] = -1;
            }

        int Answer = -987654321;

        // for (int i = 0; i < 4; i++)
        //     printf("%d %d\n", dir[i][0], dir[i][1]);
        for (int y = 0; y < N; y++)
            for (int x = 0; x < N; x++)
                Answer = max(Answer, days(y, x));

        printf("%d\n", Answer);
    }

    return 0;
}
