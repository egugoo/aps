// MST: minimum spanning tree
// KRUSKAL

#include <iostream>
#include <stdio.h>

#define MAX_T 10
#define MAX_N 15
int T, N;
int t;
float D[MAX_N][MAX_N];
float mst[MAX_T][MAX_N][MAX_N];
bool visited[MAX_N];
bool connected[MAX_N];

struct EDGE {
    int a;
    int b;
    float w;
    EDGE():a(), b(), w() {}
    EDGE(int a, int b, float w): a(a), b(b), w(w) {}
};


// EDGES array
EDGE edges[MAX_N * MAX_N / 2];
int cur_edge = 0;
void push(EDGE e) {
    edges[cur_edge++] = e;
}
EDGE pop() {
    return edges[--cur_edge];
}
bool isEmpty() {
    return cur_edge == 0;
}


bool isAllConnected() {
    for (int i = 0; i < N; i++) {
        if (!connected[i])
            return false;
    }
    return true;
}


bool isCycled(int node, int parent) {
    visited[node] = true;

    for (int i = 0; i < N; i++) {
        if (mst[t][node][i]) {
            if (!visited[i]) {
                if (isCycled(i, parent)) {
                    return true;
                }
            }
            else if (node == parent) {
                return true;
            }
        }
    }
    return false;
}

void qsort(int lo, int hi) {
    float p = edges[(lo+hi)/2].w;
    int l = lo;
    int r = hi;
    while (l < r) {
        while (edges[l].w > p) l++;
        while (edges[r].w < p) r--;
        if (l <= r) {
            EDGE tmp = edges[l];
            edges[l] = edges[r];
            edges[r] = tmp;
            l++;
            r--;
        }
    }

    if (lo < r) qsort(lo, r);
    if (l < hi) qsort(l, hi);
}

void findMst() {
    // sort edges
    qsort(0, cur_edge - 1);
    // while all connected
    int conn = 0;
    int n = N;
    while (n) connected[--n] = false;
    while (!isAllConnected()) {
        if (isEmpty()) break;
        // pick smallest edge
        EDGE e = pop();
        mst[t][e.a][e.b] = e.w;
        mst[t][e.b][e.a] = e.w;

        // confirm no cycle
        int n = N;
        while (n) visited[--n] = false;

        if (isCycled(e.a, e.a)) {
            mst[t][e.a][e.b] = 0;
            mst[t][e.b][e.a] = 0;
        } else {
            conn |= (1 << e.a);
            conn |= (1 << e.b);
        }
    }
}

int main() {
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d", &N);
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                scanf("%f", &D[i][j]);
                if (i < j && D[i][j]) {
                    push(EDGE(i, j, D[i][j]));
                }
            }
        }
        findMst();
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                printf("%f ", mst[t][i][j]);
            }
            printf("\n");
        }
    }

    return 0;
}
