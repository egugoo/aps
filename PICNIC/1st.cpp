// PICNIC
// 1st (on my way)
// 2014. 11. 09.

#include <iostream>
#include <stdio.h>

int C, n, m;
bool pairs[10][10];

int countPairing(bool *student) {
    int count = 0;
    int p1 = -1;

    for (int i = 0; i < n; ++i) {
        if (student[i]) {
            p1 = i;
            break;
        }
    }

    if (p1 == -1) return 1;

    for (int i = p1+1; i < n; ++i) {
        if (student[i] && pairs[p1][i]) {
            student[p1] = false;
            student[i] = false;
            count += countPairing(student);
            student[p1] = true;
            student[i] = true;
        }
    }
    return count;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &C);
    for (int i = 0; i < C; ++i) {
        scanf("%d %d", &n, &m);
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
                pairs[j][k] = false;
            }
        }
        for (int j = 0; j < m; ++j) {
            int p1, p2;
            scanf("%d %d", &p1, &p2);
            pairs[p1][p2] = true;
            pairs[p2][p1] = true;
        }
        bool student[10];
        for (int j = 0; j < n; ++j) {
            student[j] = true;
        }
        printf("%d\n", countPairing(student));
    }

    return 0;
}
