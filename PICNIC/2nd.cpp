// PICNIC
// 2nd (on my way)
// 2016. 05. 17.

#include <iostream>
#include <stdio.h>

int C, n, m;
bool areFriends[10][10];
bool taken[10];

int countPairings() {
    // base case
    int firstFree = -1;
    for (int i = 0; i < n; ++i) {
        if (!taken[i]) {
            firstFree = i;
            break;
        }
    }
    if (firstFree == -1) return 1;

    int ret = 0;

    for (int pairWith = firstFree + 1; pairWith < n; ++pairWith) {
        if (!taken[pairWith] && areFriends[firstFree][pairWith]) {
            taken[firstFree] = true;
            taken[pairWith] = true;
            ret += countPairings();
            taken[firstFree] = false;
            taken[pairWith] = false;
        }
    }

    return ret;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &C);
    for (int i = 0; i < C; ++i) {
        scanf("%d %d", &n, &m);
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k < n; ++k) {
                areFriends[j][k] = false;
            }
        }
        for (int j = 0; j < m; ++j) {
            int p1, p2;
            scanf("%d %d", &p1, &p2);
            areFriends[p1][p2] = true;
            areFriends[p2][p1] = true;
        }
        for (int j = 0; j < n; ++j) {
            taken[j] = false;
        }
        printf("%d\n", countPairings());
    }

    return 0;
}
