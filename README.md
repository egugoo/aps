# egugoo's APS study

## Recursive Needs

1. tree
2. root -> child

## DP Needs

1. bottom -> up
2. return value

## Template CPP

```c++
// template file

#include <iostream>
#include <stdio.h>

// sublime text shortcut
// cmd+b: build
// cmd+shift+b: run

int T, N;
int t;


int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d", &N);
        printf("%d", N);
    }

    return 0;
}

```

## qsort

```c++
#include <stdio.h>

int d[10] = {4,6,3,2,5,1,7,9,6,8};

void qsort(int lo, int hi) {
    int p = d[(lo + hi) / 2];
    int l = lo;
    int r = hi;
    while (l < r) {
        while (d[l] < p) l++;
        while (d[r] > p) r--;
        if (l <= r) {
            int t = d[l];
            d[l] = d[r];
            d[r] = t;
            l++;
            r--;
        }
    }
    if (lo < r) qsort(lo, r);
    if (l < hi) qsort(l, hi);
}

int main(int argc, char* argv[]) {
    qsort(0, 9);
    for (int i = 0; i < 10; i++) {
        printf("%d ", d[i]);
    }
    printf("\n");
    return 0;
}

```
