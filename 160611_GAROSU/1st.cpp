#include <stdio.h>

#define MAX_N 1001
#define MAX_H 1001
#define max(a,b) ((a<b)?b:a)
#define min(a,b) ((a<b)?a:b)
#define abs(a) ((a<0)?(-(a)):(a))

int T, tc, N;
int d[MAX_H][MAX_N];
int count[MAX_H];
int MaxH = -987654321;
int Cache[MAX_H][MAX_N];  // height, pos(src)

void qsort(int _d[], int lo, int hi) {
    int p = _d[(lo+hi)/2];
    int l = lo;
    int r = hi;
    while (l < r) {
        while (_d[l] < p) l++;
        while (_d[r] > p) r--;
        if (l <= r) {
            int t = _d[l];
            _d[l] = _d[r];
            _d[r] = t;
            l++;
            r--;
        }
    }
    if (lo < r) qsort(_d, lo, r);
    if (l < hi) qsort(_d, l, hi);
}

int main(void) {
    freopen("input.txt", "rt", stdin);
    // freopen("output.txt", "wt", stdout);

    scanf("%d", &T);
    for (tc = 1; tc <= T; tc++) {
        scanf("%d", &N);

        // init data, cache
        for (int i = 0; i < MAX_H; i++) {
            count[i] = 0;
            for (int j = 0; j < MAX_N; j++) {
                d[i][j] = 0;
                Cache[i][j] = 0;
            }
        }

        MaxH = -987654321;
        for (int i = 0; i < N * 2 + 2; i++) {
            int h;
            scanf("%d", &h);
            d[h][count[h]] = i % (N+1);
            count[h]++;
            MaxH = max(MaxH, h);
        }
        for (int i = 1; i <= MaxH; i++) {
            if (count[i] > 0) {
                qsort(d[i], 0, count[i]-1);
            }
        }


        d[0][0] = N;
        count[0] = 1;


        int befH = 0;
        for (int i = 0; i < N; i ++) {
            Cache[0][i] = N - i;
        }
        for (int h = 1; h <= MaxH; h++) {
            if (count[h] == 0) continue;
            for (int idx = 0; idx < count[h]; idx++) {
                int src = d[h][idx];
                Cache[h][src] = 987654321;
                for (int k = 0; k < count[befH]; k++) {
                    int dst = d[befH][k];
                    if (src < dst)
                        Cache[h][src] =
                            min(
                                Cache[h][src],

                                abs(d[h][0] - src)
                                + (d[h][count[h]-1] - d[h][0])
                                + abs(dst - d[h][count[h]-1])
                                + count[h] + Cache[befH][dst]
                            );
                    else
                        Cache[h][src] =
                            min(
                                Cache[h][src],

                                abs(d[h][0] - dst)
                                + (d[h][count[h]-1] - d[h][0])
                                + abs(src - d[h][count[h]-1])
                                + count[h] + Cache[befH][dst]
                            );
                }
            }
            befH = h;
        }

        int answer = 987654321;
        for (int i = 0; i < count[MaxH]; i++) {
            int pos = d[MaxH][i];
            answer = min(answer, pos + Cache[MaxH][pos]);
        }
        printf("#%d %d\n", tc, answer);
    }

    return 0;
}
