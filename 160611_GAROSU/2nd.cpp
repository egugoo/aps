#include <stdio.h>

#define L 0
#define R 1
#define C 2

#define min(a,b) ((a<b)?a:b)
#define max(a,b) ((a<b)?b:a)
#define abs(a) ((a<0)?(-(a)):(a))

int T, t, N;
int d[1001][3];

int main(void) {
    freopen("input.txt", "rt", stdin);
    scanf("%d", &T);
    int input;
    for (int t = 1; t <= T; t++) {
        int woods = 0;
        scanf("%d", &N);
        for (int i = 1; i < 1001; i++) {
            d[i][L] = 987654321;
            d[i][R] = -1;
            d[i][C] = 0;
        }
        for (int j = 0; j < 2; j++) {
            for (int i = 0; i <= N; i++) {
                scanf("%d", &input);
                if (input > 0) {
                    d[input][L] = min(d[input][L], i);
                    d[input][R] = max(d[input][R], i);
                    d[input][C]++;
                    woods++;
                }
            }
        }
        d[0][L] = N;
        d[0][R] = N;
        d[0][C] = 1;
        int pL = 0; // prev left
        int pR = 0;
        int pLV = 0; // prev left value
        int pRV = 0;
        for (int i = 1000; i >= 0; i--) {
            if (d[i][C] > 0) {
                // l l: (pl cr cl)
                // r l: (pr cr cl)
                int ll = pLV + abs(pL - d[i][R]);
                int rl = pRV + abs(pR - d[i][R]);
                // l r: (pl cl cr)
                // r r: (pr cl cr)
                int lr = pLV + abs(pL - d[i][L]);
                int rr = pRV + abs(pR - d[i][L]);
                pLV = min(ll, rl) + d[i][R] - d[i][L];
                pRV = min(lr, rr) + d[i][R] - d[i][L];
                pL = d[i][L];
                pR = d[i][R];
            }
        }
        printf("#%d %d\n", t, min(pLV, pRV) + woods);
    }

    return 0;
}
