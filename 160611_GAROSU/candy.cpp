#include <stdio.h>

#define max(a,b) ((a<b)?b:a)
#define min(a,b) ((a<b)?a:b)
#define abs(a) ((a<0)?(-(a)):(a))

int data[1001][3]; // 0 : left, 1 : right, 2 : count
int woodCount;

int main(void) {
    freopen("input.txt", "rt", stdin);

    int T, N, input;

    scanf("%d", &T);
    for (int t = 1; t <= T; t++)
    {
        woodCount = 0;
        scanf("%d", &N);
        data[0][0] = N;
        data[0][1] = N;
        data[0][2] = 1;
        for (int i = 1; i <= 1000; i++)
        {
            data[i][0] = 987654321;
            data[i][1] = -1;
            data[i][2] = 0;
        }
        for (int j = 0; j < 2; j++)
        {
            for (int i = 0; i <= N; i++)
            {
                scanf("%d", &input);
                if (input > 0)
                {
                    woodCount++;
                    if (i < data[input][0]) data[input][0] = i;
                    if (i > data[input][1]) data[input][1] = i;
                    data[input][2]++;
                }
            }
        }

        int prevMin = 0;
        int prevMax = 0;
        int prevMinValue = 0;
        int prevMaxValue = 0;

        for (int i = 1000; i >= 0; i--)
        {
            if (data[i][2] > 0)
            {
                // l -> l (pl -> cr -> cl)
                int ll = prevMinValue + abs(prevMin - data[i][1]) + abs(data[i][1] - data[i][0]);
                // r -> l (pr -> cr -> cl)
                int rl = prevMaxValue + abs(prevMax - data[i][1]) + abs(data[i][1] - data[i][0]);

                // l -> r (pl -> cl -> cr)
                int lr = prevMinValue + abs(prevMin - data[i][0]) + abs(data[i][1] - data[i][0]);
                // r -> r (pr -> cl -> cr)
                int rr = prevMaxValue + abs(prevMax - data[i][0]) + abs(data[i][1] - data[i][0]);

                prevMinValue = min(ll, rl);
                prevMaxValue = min(lr, rr);

                prevMin = data[i][0];
                prevMax = data[i][1];
            }
        }
        printf("#%d %d\n", t, min(prevMinValue, prevMaxValue) + woodCount);
    }

    return 0;
}
