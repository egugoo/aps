#include <stdio.h>
#include <stdlib.h>

int main(void) {
    srand(2);

    freopen("input.txt", "wt", stdout);

    printf("%d\n", 50);
    for (int t = 0; t < 50; t++) {
        int N = rand() % 1001;
        printf("%d\n", N);
        printf("%d", 0);
        for (int i = 0; i < N; i++) {
            printf(" %d", rand() % 1001);
        }
        printf("\n");
        printf("%d", 0);
        for (int i = 0; i < N; i++) {
            printf(" %d", rand() % 1001);
        }
        printf("\n");
    }
    return 0;
}
