// BOGGLE
// 1st (on my way)
// 2014. 11. 09.

#include <iostream>
#include <stdio.h>

// 1
// URLPM
// XPRET
// GIAET
// XTNZY
// XOQRS
// 6
// PRETTY
// GIRL
// REPEAT
// KARA
// PANDORA
// GIAZAPX

char grid[5][5] = {0, };
char word[10240000] = {0,};

const int dx[8] = {-1, 0, 1, -1, 1, -1, 0, 1};
const int dy[8] = {-1, -1, -1, 0, 0, 1, 1, 1};

bool isThereNextLetter(int cur, int x, int y, int remain) {
    bool ret = false;
    if (x < 0 || y < 0 || x > 5 || y > 5) return false;
    if (word[cur] == grid[x][y]) {
        if (remain == 0) {
            return true;
        }
        for (int dir = 0; dir < 8; ++dir) {
            if (isThereNextLetter(cur+1, x + dx[dir], y + dy[dir], remain-1)) {
                ret = true;
            }
        }
    }
    return ret;
}

int main()
{
    int C, N;

    freopen("input.txt", "rt", stdin);
    scanf("%d", &C);
    getchar();
    for (int c = 0; c < C; ++c) {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                grid[i][j] = getchar();
            }
            getchar();
        }

        scanf("%d", &N);
        for (int n = 0; n < N; ++n) {
            scanf("%s", word);
            printf("%s ", word);
            int length = 0;
            bool exist = false;
            while (word[length]) ++length;
            for (int i = 0; i < 5 * 5; ++i) {
                if (grid[i/5][i%5] == word[0] && isThereNextLetter(0, i/5, i%5, length-1)) {
                    printf("YES\n");
                    exist = true;
                    break;
                }
            }
            if (!exist) printf("NO\n");
        }
    }

    return 0;
}
