// BOGGLE
// 2nd (book way)
// 2016. 05. 17.

#include <iostream>
#include <stdio.h>

// 1
// URLPM
// XPRET
// GIAET
// XTNZY
// XOQRS
// 6
// PRETTY
// GIRL
// REPEAT
// KARA
// PANDORA
// GIAZAPX

char board[5][5] = {0, };
char word[10240000] = {0,};

int dx[8] = {-1, -1, -1,  1, 1, 1,  0, 0};
int dy[8] = {-1,  0,  1, -1, 0, 1, -1, 1};

#define inRange(y, x) (0 <= x && x < 5 && 0 <= y && y < 5)

bool hasWord(int y, int x, char* word) {
    // base case
    // 1. in range
    if (!inRange(y, x)) return false;
    // 2. first letter
    if (board[y][x] != word[0]) return false;
    // 3. word length
    if (word[1] == 0) return true;

    // check 8 directions
    for (int dir = 0; dir < 8; ++dir)
        if (hasWord(y + dy[dir], x + dx[dir], word + 1))
            return true;

    return false;
}

int main()
{
    int C, N;

    freopen("input.txt", "rt", stdin);
    scanf("%d", &C);
    getchar();
    for (int c = 0; c < C; ++c) {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                board[i][j] = getchar();
            }
            getchar();
        }

        scanf("%d", &N);
        for (int n = 0; n < N; ++n) {
            scanf("%s", word);
            printf("%s ", word);
            bool exist = false;
            for (int y = 0; y < 5; ++y) {
                for (int x = 0; x < 5; ++x) {
                    if (hasWord(y, x, word)) {
                        exist = true;
                        break;
                    }
                }
                if (exist) {
                    printf("YES\n");
                    break;
                }
            }
            if (!exist) printf("NO\n");
        }
    }

    return 0;
}
