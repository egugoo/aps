// TRIANGLEPATH
// 2nd

#include <stdio.h>

#define MAX_N 100

#define MAX(a,b) ((a>b)?a:b)

int T, N;
int t;
int data[MAX_N][MAX_N] = { 0, };


// up to down
void findAnswer(int y, int x, int ans) {
    ans += data[y][x];

    if (y == N - 1) {
        if (ans > answer) answer = ans;
        return;
    }

    findAnswer(y + 1, x, ans);
    findAnswer(y + 1, x + 1, ans);
}

// bottom up
int findAnswer(int y, int x) {
    if (y == N - 1) return data[y][x]; // last y, 1st x

    int &ret = cache[t][y][x];
    if (ret > 0) return ret;

    ret = data[y][x] + MAX(findAnswer(y+1, x), findAnswer(y+1, x+1));

    return ret;
}


int main() {
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {

        scanf("%d", &N);

        scanf("%d", &(data[0][0]));

        int max = -987654321;

        for (int j = 1; j < N; j++) {
            for (int k = 0; k <= j; k++) {
                scanf("%d", &(data[j][k]));
                int left = (k > 0) ? data[j-1][k-1] : 0;
                int right = (k == j) ? 0 : data[j - 1][k];
                data[j][k] += (left > right) ? left : right;

                if (data[j][k] > max)
                    max = data[j][k];
            }
        }

        printf("%d\n", max);
    }

    return 0;
}
