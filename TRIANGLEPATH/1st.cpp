// TRIANGLEPATH
// 1st

#include <stdio.h>

#define MAX_N 100

int C, N = 0;
int tri[MAX_N][MAX_N] = { 0, };

int main() {
    freopen("input.txt", "rt", stdin);

    scanf("%d", &C);

    for (int i = 0; i < C; i++) {

        scanf("%d", &N);

        scanf("%d", &(tri[0][0]));

        int max = -987654321;

        for (int j = 1; j < N; j++) {
            for (int k = 0; k <= j; k++) {
                scanf("%d", &(tri[j][k]));
                int left = (k > 0) ? tri[j-1][k-1] : 0;
                int right = (k == j) ? 0 : tri[j - 1][k];
                tri[j][k] += (left > right) ? left : right;

                if (tri[j][k] > max)
                    max = tri[j][k];
            }
        }

        printf("%d\n", max);
    }

    return 0;
}
