// TRIANGLEPATH
// 3rd

#include <stdio.h>

#define MAX_C 50
#define MAX_N 100

#define max(a,b) ((a>b)?a:b)


int T, N;
int t;
int D[MAX_N][MAX_N];
int DP[MAX_C][MAX_N][MAX_N];


int findAnswer(int y, int x) {
    if (y == N - 1) return D[y][x];

    int &ret = DP[t][y][x];
    if (ret) return ret;

    ret = D[y][x] + max(findAnswer(y + 1, x), findAnswer(y + 1, x + 1));

    return ret;
}


int main() {
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d", &N);

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < i + 1; j++) {
                scanf("%d", &D[i][j]);
            }
        }

        printf("%d\n", findAnswer(0, 0));
    }

    return 0;
}
