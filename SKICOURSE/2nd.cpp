#include <iostream>
#include <stdio.h>

#define MAX_T 100
#define MAX_N 50001
#define MAX_M 100001
#define MAX_S 101
#define max(a,b) ((a>b)?a:b)

int T, N, M, S;
int t;
int data[MAX_T][MAX_N][MAX_N];

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d %d %d", &N, &M, &S);
        int answer = 0;
        for (int m = 0; m < M; m++) {
            int a, b, c;
            scanf("%d %d %d", &a, &b, &c);
            for (int s = 1; s <= S; ++s) {
                data[t][b][s-1] = max(data[t][b][s-1], data[t][a][s]+c);
                answer = max(answer, data[t][b][s-1]);
            }
        }
        printf("%d\n", answer);
    }

    return 0;
}

