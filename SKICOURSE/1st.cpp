#include <iostream>
#include <stdio.h>

#define MAX_T 100
#define MAX_N 50001
#define MAX_M 100001
#define MAX_S 101
#define max(a,b) ((a>b)?a:b)

int T, N, M, S;
int t;
int data[MAX_T][MAX_N][MAX_N];

int findAnswer(int start, int passed, int sum) {
    int answer = sum;
    if (passed < S) {
        for (int i = start+1; i <= N; i++) {
            if (data[t][start][i] > 0) {
                answer = max(answer, findAnswer(i, passed+1, sum + data[t][start][i]));
            }
        }
    }
    return answer;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d %d %d", &N, &M, &S);
        for (int m = 0; m < M; m++) {
            int a, b, c;
            scanf("%d %d %d", &a, &b, &c);
            data[t][a][b] = c;
        }
        int maxFun = 0;
        for (int i = 1; i <= N; i++) {
            maxFun = max(maxFun, findAnswer(i, 0, 0));
        }
        printf("%d\n", maxFun);
    }

    return 0;
}
