#include <stdio.h>
#define MAX_N 250
#define Q 100000000

int TC, N;
struct BI  // Big Integer
{
    int d[15];

    BI()
    {
        for (int i = 0; i < 15; i++)
            d[i] = 0;
    }

    BI(int a)
    {
        d[0] = a;
        for (int i = 1; i < 15; i++)
            d[i] = 0;
    }

    BI& operator=(const BI& a)
    {
        for (int i = 0; i < 15; i++)
        {
            d[i] = a.d[i];
        }
        return *this;
    }

    BI operator+(const BI& a) const
    {
        BI n = BI(0);
        int c = 0;
        for (int i = 0; i < 15; i++)
        {
            int b = c + d[i] + a.d[i];
            c = b / Q;
            n.d[i] = b % Q;
        }
        return n;
    }
};

BI DP[MAX_N+1];

void putsBig(BI a)
{
    bool begin = false;
    for (int i = 14; i >= 0; i--)
    {
        if (begin)
            printf("%08d", a.d[i]);
        else if (a.d[i] != 0)
        {
            printf("%d", a.d[i]);
            begin = true;
        }
    }
    if (!begin) printf("0");
}

int main()
{
    // freopen("8-5.txt", "rt", stdin);
    freopen("8-5.input.txt", "rt", stdin);
    scanf("%d", &TC);

    DP[0] = BI(0);
    DP[1] = BI(1);
    DP[2] = BI(3);
    for (int i = 3; i <= 250; i++)
    {
        DP[i] = DP[i-1] + DP[i-2] + DP[i-2];
    }

    for (int tc = 1; tc <= TC; tc++)
    {
        scanf("%d", &N);
        // printf("#%d %d\n", tc, DP[N]);
        printf("#%d ", tc);
        putsBig(DP[N]);
        printf("\n");
    }

    return 0;
}
