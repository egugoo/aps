#include <stdio.h>

int TC, T, N, K;
int Answer;
int D[100000];

bool Picked[100000];
int Hole[100000];
// bool hole = false;
int count = 0;
int first = 0;

void printPick(int idx) {
  for (int i = 0; i <= idx; i++) {
    if (Picked[i])
      printf("%d ", D[i]);
    else
      printf("- ");
  }
  printf("\n");
}

void printHole(int idx) {
  for (int i = 0; i <= idx; i++) {
    if (Hole[i] > 0)
      printf("(%d)", D[i]);
    else if (Hole[i] < 0)
      printf(" - ");
    else
      printf(" %d ", D[i]);
  }
  printf("\n");
}

void hole(int i) {
  if (i >= N) return;
  if (D[i-1] > D[i]) return;
  if (i - first + 1 >= K) {
    Answer++;
    hole(i+1);
  }
}

void je(int i) {
  // return check
  if (i >= N) return;

  if (D[i-1] > D[i]) {
    // end like .. 7 5i
    if (i - first + 1 >= K) Answer++;
    if (i + 1 < N && D[i-1] < D[i+1]) {
      // continue like .. 7 5i 8
      if (i - first + 1 >= K) Answer++;
      hole(i+2);
    }
    return;
  }
  else {
    if (i - first + 1 >= K) Answer++;
    je(i+1);
  }
}


// void je(int i) {
//   // return check
//   if (i >= N) return;
//   // if (i > 0 && Picked[i - 1] && D[i-1] > D[i]) return;

//   // no pick
//   // je i + 1
//   je(i+1);

//   // pick
//   // je i + 1
//   // i > K -> count

//   // count = 1;
//   // while (i >= count && Hole[i - count]) count++;
//   Hole[i] = 0;
//   // if (count >= K) {
//   printHole(i);
//   Answer++;
//   // }
//   je(i+1);
//   Hole[i] = -1;
// }

// void je(int i) {
//   // return check
//   if (i >= N) return;
//   // if (i > 0 && Picked[i - 1] && D[i-1] > D[i]) return;

//   first = 0;
//   while (first < i && !Picked[first]) first++;

//   // no pick
//   // je i + 1
//   je(i+1);

//   // pick
//   // je i + 1
//   // i > K -> count
//   if (i > 2 && !Picked[i-1]) {
//     // .... - 9i
//     if (Picked[i-2]) {
//       // .... 9 - 9i
//       if (D[i-2] > D[i]) return;
//       count = i - 2;
//       while (count >= 0 && Picked[count]) count--;
//       for (count--; count >= 0; count--) {
//         if (Picked[count]) return;
//       }
//     }
//     else {
//       // .... - - 9i
//       for (count = i-3; count >= 0; count--) {
//         if (Picked[count]) return;
//       }
//     }
//   }
//   else {
//     // .... 9 9i
//     if (D[i-1] > D[i]) {
//       if (i - first + 1 >= K) {
//         printPick(i);
//         Answer++;
//       }
//       return;
//     }
//   }
//   count = 1;
//   while (i >= count && Picked[i - count]) count++;
//   Picked[i] = true;
//   if (count >= K) {
//     printPick(i);
//     Answer++;
//   }
//   je(i+1);
//   Picked[i] = false;
// }

int main() {

  setbuf(stdout, NULL);
  freopen("input_1_big.txt", "rt", stdin);

  scanf("%d", &TC);

  for (T = 1; T <= TC; T++) {
    Answer = 0;

    scanf("%d %d", &N, &K);

    for (int i = 0; i < N; i++) {
      scanf("%d", &D[i]);
      Picked[i] = false;
      Hole[i] = -1;
    }

    for (int i = 0; i < N; i++) {
      first = i;
      je(i+1);
    }

    printf("#%d %d\n", T, Answer);
  }

  return 0;
}
