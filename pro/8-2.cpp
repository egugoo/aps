#include <stdio.h>
#define MAX_N 1000
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))

char Image[MAX_N][MAX_N+1];
int D[MAX_N][MAX_N];

int TC, N;

int main()
{
    // freopen("8-2.txt", "rt", stdin);
    // freopen("8-2.input.txt", "rt", stdin);
    scanf("%d", &TC);

    for (int t = 1; t <= TC; t++)
    {
        scanf("%d", &N);
        int ans = 0;
        for (int i = 0; i < N; i++)
        {
            scanf("%s", Image[i]);
        }

        for (int y = 0; y < N; y++)
        {
            for (int x = 0; x < N; x++)
            {
                if (Image[y][x] == '1')
                    D[y][x] = 0;
                else if (y == 0 || x == 0)
                    D[y][x] = 1;
                else
                    D[y][x] = min(D[y-1][x-1], min(D[y-1][x], D[y][x-1])) + 1;
                ans = max(ans, D[y][x]);
            }
        }

        printf("#%d %d\n", t, ans);
    }

    return 0;
}
