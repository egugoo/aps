#include <stdio.h>
#define MAX_N 50000
#define MAX_M 200000

typedef struct
{
    int s;
    int e;
    int w;
} Edge;

Edge E[MAX_M+1];
int DJSet[MAX_N+1][2];

int TC, N, M;

void merge(Edge *A, Edge *L, Edge *R, int l, int r)
{
    int i,j,k;
    i = 0; j = 0; k = 0;
    while (i < l && j < r)
    {
        if (L[i].w < R[j].w) A[k++] = L[i++];
        else A[k++] = R[j++];
    }
    while (i < l) A[k++] = L[i++];
    while (j < r) A[k++] = R[j++];
}

void msort(Edge *A, int n)
{
    if (n < 2) return;
    int m = n / 2;
    Edge *L = new Edge[m];
    Edge *R = new Edge[n-m];
    for (int i = 0; i < m; i++) L[i] = A[i];
    for (int i = m; i < n; i++) R[i-m] = A[i];
    msort(L, m);
    msort(R, n-m);
    merge(A, L, R, m, n-m);
    delete[] L;
    delete[] R;
}

int Find(int n)
{
    if (DJSet[n][0] == n) return n;
    return DJSet[n][0] = Find(DJSet[n][0]);
}

void Union(int a, int b)
{
    int pA = Find(DJSet[a][0]);
    int pB = Find(DJSet[b][0]);
    int rankA = DJSet[pA][1];
    int rankB = DJSet[pB][1];
    if (rankA == rankB)
    {
        DJSet[pB][0] = pA;
        DJSet[pA][1]++;
    }
    else if (rankA < rankB)
        DJSet[pA][0] = pB;
    else
        DJSet[pB][0] = pA;
}

int main()
{
    freopen("5-2.txt", "rt", stdin);
    // freopen("5-2.input.txt", "rt", stdin);
    scanf("%d", &TC);

    for (int tc = 1; tc <= TC; tc++)
    {
        scanf("%d %d", &N, &M);

        for (int i = 0; i < M; i++)
        {
            scanf("%d %d %d", &E[i].s, &E[i].e, &E[i].w);
        }

        for (int i = 0; i <= N; i++)
        {
            // init set
            DJSet[i][0] = i;
            DJSet[i][1] = 0;
        }

        msort(E, M);

        // for (int i = 0; i <= N; i++)
        //     printf("%d %d %d\n", i, DJSet[i][0], DJSet[i][1]);

        int Ans = 0;
        for (int i = 0; i < M; i++)
        {
            if (Find(E[i].s) != Find(E[i].e))  // no cycle
            {
                Union(E[i].s, E[i].e);
                Ans += E[i].w;
            }
        }

        // for (int i = 0; i <= N; i++)
        //     printf("%d %d %d\n", i, DJSet[i][0], DJSet[i][1]);

        // printf("\n");
        printf("#%d %d\n", tc, Ans);
    }
    return 0;
}
