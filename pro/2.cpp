#include <stdio.h>

int TC, T, N, K;
int Answer;
int Data[100001][100001][2];
// int Data2[100001][100001][100002];
int Keis[100001][2];

int main() {

    setbuf(stdout, NULL);
    freopen("input_2.txt", "rt", stdin);
    scanf("%d", &TC);

    for (T = 1; T <= TC; T++) {
        scanf("%d %d", &N, &K);

        for (int i = 1; i <= N; i++)
            for (int j = 1; j <= N; j++) {
                if (i == j) Data[i][j][0] = 0;
                else Data[i][j][0] = 987654321;
                // Data[i][j][1] = -1;
            }

        int p, c; // parent, child
        for (int i = 0; i < N-1; i++) {
            scanf("%d %d", &p, &c);
            Data[p][c][0] = 1;
            Data[c][p][0] = 1;
            Data[p][c][1] = c;
            Data[c][p][1] = c;
        }

        for (int i = 1; i <= N; i++) {
            for (int j = 1; j <= N; j++) {
                for (int k = 1; k <= N; k++) {
                    if (Data[k][j][0] > Data[k][i][0] + Data[i][j][0]) {
                        Data[k][j][0] = Data[k][i][0] + Data[i][j][0];
                        Data[k][j][1] = Data[i][j][1];
                    }
                }
            }
        }

        // for (int i = 1; i <= N; i++) {
        //     for (int j = 1; j <= N; j++) {
        //         printf("%2d ", Data[i][j][1]);
        //     }
        //     printf("\n");
        // }

        for (int i = 0; i < K; i++) {
            scanf("%d %d", &Keis[i][0], &Keis[i][1]);
        }

        // int sum = 0;
        // for (int i = 1; i <= N; i++) {
        //     for (int j = i; j <= N; j++) {
        //         if (Data[i][j][1] == -1) continue;
        //         printf("(%d > %d):", i, j);
        //         int pre = j;
        //         int count = 0;
        //         Data2[i][j][count++] = j;
        //         while (i != Data[i][pre][1]) {
        //             pre = Data[i][pre][1];
        //             Data2[i][j][count++] = pre;
        //         }
        //         Data2[i][j][count] = -1;
        //         printf("%d", i + 1);
        //         for (int k = 1; k < count; k++) {
        //             printf("-%d", Data2[i][j][k]);
        //         }
        //         printf("\n");
        //     }
        // }

        int sum = 0;
        for (int i = 1; i <= N; i++)
            for (int j = i + 1; j <= N; j++) {
                sum += Data[i][j][0];
            }

        printf("#%d %d\n", T, sum * 2);
    }

    return 0;
}
