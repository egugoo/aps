#include <stdio.h>

int TC, T, N, K;
int Answer;
int D[100000];

int main() {

  setbuf(stdout, NULL);
  freopen("input_1_big.txt", "rt", stdin);

  scanf("%d", &TC);

  for (T = 1; T <= TC; T++) {
    Answer = 0;

    scanf("%d %d", &N, &K);

    for (int i = 0; i < N; i++) {
      scanf("%d", &D[i]);
    }

    for (int i = 0; i < N; i++) {
      int hole = -1;
      for (int j = i + 1; j < N; j++) {

        if (j - i + 1 >= K) {
          // enough length
          if (hole < 0) {
            // no hole
            if (D[j-1] < D[j] ) {
              Answer++;
            } else {
              hole = j;
              Answer++;
            }
          }
          else {
            // has hole
            if (hole != j - 1 && D[j-1] < D[j]) Answer++;
            else if (hole == j - 1 && D[j-2] < D[j]) Answer++;
            else break;
          }

        } else {
          // not enough length: hole check
          if (D[j-1] > D[j]) {
            if (hole > 0) {
              i = hole - 1;
              hole = -1;
              break;
            }
            hole = j;
          }
        }

      }
    }
    printf("#%d %d\n", T, Answer);
  }

  return 0;
}
