#include <stdio.h>
#define MAX_L 1000

int TC;
int L;
char S[MAX_L+1];
char D[MAX_L+1];
int I[MAX_L+1];
int StackIdx[MAX_L+1];
int nStack = 0;

void push(int n)
{
    StackIdx[nStack] = n;
    nStack++;
}

void pop()
{
    nStack--;
}

bool isEmpty()
{
    return nStack == 0;
}

int main()
{
    // freopen("2-3.txt", "rt", stdin);
    freopen("2-3.input.txt", "rt", stdin);
    setbuf(stdout, NULL);
    scanf("%d", &TC);
    for (int t = 1; t <= TC; t++)
    {
        scanf("%d", &L);
        scanf("%s", S);

        if (L & 1)
        {
            printf("#%d -1\n", t);
        }
        else
        {
            int k = 0;
            nStack = 0;
            for (int i = 0; i < L; i++)
            {
                if (S[i] == '(') push(i);
                else
                {
                    if (isEmpty()) { I[k] = i; D[k++] = ')'; }
                    else pop();
                }
            }
            int close = k - 1;
            for (int i = 0; i < nStack; i++, k++)
            {
                I[k] = StackIdx[i];
                D[k] = '(';
            }
            D[k] = 0; // end of string
            printf("#%d ", t);
            if (k == 0) printf("0\n");  // perfact ()
            else
            {
                if (close == -1)
                {
                    printf("1\n%d %d\n", I[k/2], L-1);
                }
                else
                {
                    printf("2\n%d %d\n", 0, I[close]);
                    int closeIndex = I[close];
                    for (int i = close; i >= 0; i--)
                        StackIdx[close - i] = closeIndex - I[i];
                    for (int i = 0; i < close + 1; i++)
                        I[i] = StackIdx[i];
                    printf("%d %d\n", I[k/2], L-1);
                }
            }
        }
    }
    return 0;
}
