#include <stdio.h>
#define MAX_N 10

#define max(a,b) (a>b?a:b)

int D[MAX_N][MAX_N];
bool Visit[MAX_N];
int TC, N, K, M;
int Ans;

// INPUT
// TC
// N K
// M1 m m m m m
// M2 m m m m m m m
// ...

void dfs(int v, int depth)
{
    Visit[v] = true;
    Ans = max(Ans, depth);
    for (int i = 0; i < N; i++)
    {
        if (D[v][i] == 1 && !Visit[i])
        {
            dfs(i, depth + 1);
            Visit[i] = false;
        }
    }
}


int main()
{
    freopen("5-1.input.txt", "rt", stdin);
    scanf("%d", &TC);

    for (int tc = 1; tc <= TC; tc++)
    {
        scanf("%d %d", &N, &K);

        // init
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                D[i][j] = 0;

        // fill matrix
        for (int k = 0; k < K; k++)
        {
            int src, dst;
            scanf("%d %d", &M, &src);
            for (int m = 1; m < M; m++)
            {
                scanf("%d", &dst);
                D[src-1][dst-1] = 1;
                src = dst;
            }
        }

        printf("#%d", tc);
        for (int i = 0; i < N; i++)
        {
            int cnt = 0;
            for (int j = 0; j < N; j++) if (D[i][j] == 1) cnt++;
            printf(" %d", cnt);
        }

        Ans = 0;
        for (int i = 0; i < N; i++)
        {
            for (int i = 0; i < N; i++) Visit[i] = false;
            dfs(i, 0);
        }

        printf(" %d\n", Ans + 1);
    }
    return 0;
}
