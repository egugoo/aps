#include <stdio.h>

int TC, T, N;
int Answer;
char D[1000][4];
bool Picked[1000];
#define max(a,b) (((a)>(b))?(a):(b))

void pick(int n, int sum) {
    if (n >= N) return;
    pick(n + 1, sum);
    if (Picked[n]) return pick(n + 1, sum);

    Picked[n] = true;
    if (D[n][0] != '*') {
        if (D[n][1] != '*') {
            if (D[n][2] != '*') {
                // 000
                sum += D[n][0] - '0';
                sum += D[n][1] - '0';
                sum += D[n][2] - '0';
                Answer = max(Answer, sum);
                pick(0, sum);
            }
            else {
                // 00*
                sum += D[n][0] - '0';
                sum += D[n][1] - '0';
                Answer = max(Answer, sum);
            }
        }
        else {
            if (D[n][2] != '*') {
                // 0*0
                sum += D[n][0] - '0';
                Answer = max(Answer, sum);
                sum = D[n][2] - '0';
                pick(0, sum);
            }
            else {
                // 0**
                sum += D[n][0] - '0';
                Answer = max(Answer, sum);
            }
        }
    }
    else if (D[n][1] != '*') {
        if (D[n][2] != '*') {
            // *00
            sum = D[n][1] - '0';
            sum += D[n][2] - '0';
            Answer = max(Answer, sum);
            pick(0, sum);
        }
        else {
            // *0*
            Answer = max(Answer, D[n][1] - '0');
        }
    }
    else if (D[n][2] != '*') {
        // **0
        sum = D[n][2] - '0';
        Answer = max(Answer, sum);
        pick(0, sum);
    }
    else {
        // ***
    }
    Picked[n] = false;
    return;
}

int main() {

    setbuf(stdout, NULL);
    freopen("input_3.txt", "rt", stdin);
    scanf("%d", &TC);

    for (T = 1; T <= TC; T++) {
        Answer = 0;
        scanf("%d", &N);

        for (int i = 0; i < N; i++) {
            scanf("%s", D[i]);
            Picked[i] = false;
        }

        pick(0, 0);
        printf("#%d %d\n", T, Answer);
    }

    return 0;
}
