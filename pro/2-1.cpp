#include <stdio.h>
#define MAX_N 200000

int TC, N;

typedef struct
{
    int a;
    int b;
    float ratio;
} Datum;

Datum D[MAX_N];

void merge(Datum *A, Datum *L, int nL, Datum *R, int nR)
{
    int i, j, k;
    i = 0; j = 0; k = 0;
    while (i < nL && j < nR)
    {
        if (L[i].ratio > R[j].ratio) A[k++] = L[i++];
        else A[k++] = R[j++];
    }
    while (i < nL) A[k++] = L[i++];
    while (j < nR) A[k++] = R[j++];
}

void msort(Datum *A, int n)
{
    int mid, i;
    Datum *L, *R;
    if (n < 2) return;

    mid = n / 2;

    L = new Datum[mid + 1];
    R = new Datum[mid + 1];

    for (i = 0; i < mid; i++) L[i] = A[i];
    for (i = mid; i < n; i++) R[i-mid] = A[i];

    msort(L, mid);
    msort(R, n-mid);
    merge(A, L, mid, R, n-mid);

    delete[] L;
    delete[] R;
}

int main()
{
    freopen("2-1.input.txt", "rt", stdin);
    setbuf(stdout, NULL);
    scanf("%d", &TC);

    for (int t = 1; t <= TC; t++)
    {
        scanf("%d", &N);

        for (int i = 0; i < N; i++)
        {
            scanf("%d %d", &D[i].a, &D[i].b);
            D[i].ratio = ((float)D[i].a - 1) / D[i].b;
        }
        msort(D, N);
        long long v = 1;
        for (int i = 0; i < N; i++)
        {
            v = ((D[i].a * v) + D[i].b) % 1000000007;
        }
        printf("#%d %lld\n", t, v);
    }

    return 0;
}
