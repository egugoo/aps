#include <stdio.h>
#define MAX_N 500
#define MAX_M 10000
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))

int TC, N, M;
typedef struct
{
    int s;
    int e;
    int c;
} Work;
Work W[MAX_N];

int DP[MAX_N];

void merge(Work *A, Work *L, Work *R, int l, int r)
{
    int i, j, k;
    i=0; j=0; k=0;
    while (i < l && j < r)
    {
        if (L[i].e <= R[j].e) {
            if (L[i].e == R[j].e)
                A[k++] = (L[i].s < R[j].s) ? L[i++] : R[j++];
            else
                A[k++] = L[i++];
        }
        else
            A[k++] = R[j++];
    }
    while (i < l) A[k++] = L[i++];
    while (j < r) A[k++] = R[j++];
}

void msort(Work *A, int n)
{
    if (n < 2) return;

    int m = n / 2;
    Work *L = new Work[m];
    Work *R = new Work[n-m];

    for (int i=0; i<m; i++) { L[i] = A[i]; }
    for (int i=m; i<n; i++) { R[i-m] = A[i]; }

    msort(L, m);
    msort(R, n-m);
    merge(A, L, R, m, n-m);

    delete[] L;
    delete[] R;
}

int main()
{
    // freopen("8-1.txt", "rt", stdin);
    freopen("8-1.input.txt", "rt", stdin);
    scanf("%d", &TC);

    for (int tc = 1; tc <= TC; tc++)
    {
        scanf("%d %d", &N, &M);
        for (int i = 0; i < N; i++)
        {
            scanf("%d %d %d", &W[i].s, &W[i].e, &W[i].c);
        }

        msort(W, N);

        DP[0] = 0;
        for (int i = 1; i <= N; i++)
        {
            // get nearest end work
            int st = 0;
            int en = i-1;
            int want = W[i-1].s;
            int ans = 0;
            while (st <= en)
            {
                int mi = (st+en)/2;
                if (W[mi-1].e < want)
                {
                    st = mi + 1;
                    ans = DP[mi];
                }
                else
                {
                    en = mi - 1;
                }
            }
            DP[i] = max(DP[i-1], W[i-1].c + ans);
        }
        // for (int i = 0; i <= N; i++)
        //     printf("%d ", DP[i]);

        printf("#%d %d\n", tc, DP[N]);
    }

    return 0;
}
