#include <stdio.h>
#define MAX_N 500
#define INF 987654321
#define min(a,b) ((a)<(b)?(a):(b))

int D[MAX_N][MAX_N];
int TC, N, M;

void showD()
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (D[i][j] == INF) printf("  INF ");
            else printf("%5d ", D[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main()
{
    freopen("8-4.txt", "rt", stdin);
    // freopen("8-4.input.txt", "rt", stdin);
    freopen("8-5.output.txt", "wt", stdout);
    scanf("%d", &TC);

    for (int t = 1; t <= TC; t++)
    {
        scanf("%d %d", &N, &M);
        // init
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                D[i][j] = INF;
        for (int m = 0; m < M; m++)
        {
            int a, b, c;
            scanf("%d %d %d", &a, &b, &c);
            D[a-1][b-1] = c;
        }
        // init self path
        for (int i = 0; i < N; i++)
            D[i][i] = 0;

        // all pair
        for (int n = 0; n < N; n++)
        {
            for (int i = 0; i < N; i++)
                for (int j = 0; j < N; j++)
                    if (D[i][n] != INF && D[n][j] != INF)
                        D[i][j] = min(D[i][j], D[i][n] + D[n][j]);
            // showD();
        }

        // init self path
        for (int i = 0; i < N; i++)
            D[i][i] = 0;

        printf("#%d", t);
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                if (D[i][j] == INF) printf(" -1");
                else printf(" %d", D[i][j]);
        printf("\n");
    }

    return 0;
}
