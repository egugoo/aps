// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define MAX_N 1001
#define MAX_W 1002
#define CAR1 1000
#define CAR2 1001
#define MOVE_AGAIN 0
#define MOVE_OTHER 1

#define min(a,b) ((a<b)?a:b)
#define abs(a) (((a)<0)?(-(a)):(a))

int C[MAX_W][MAX_W];
int D[MAX_W][2];
int Choice[MAX_W][MAX_W];

int T, N, W;
int t;

int getDist(int a, int b) {
    return abs(D[a][0] - D[b][0]) + abs(D[a][1] - D[b][1]);
}

int minWork(int carPos, int workNum) {
    // base case
    if (workNum >= W-1) return 0;
    int &ret = C[carPos][workNum];

    if (ret >= 0) return ret;

    // move again
    int a = getDist(workNum, workNum+1) + minWork(carPos, workNum+1);
    // move another car
    int b = getDist(carPos, workNum+1) + minWork(workNum, workNum+1);
    if (a<b) {
        Choice[carPos][workNum] = MOVE_AGAIN;
    } else {
        Choice[carPos][workNum] = MOVE_OTHER;
    }
    ret = min(a,b);

    return ret;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    // scanf("%d", &T);
    T = 1;

    for (t = 0; t < T; t++) {
        scanf("%d %d", &N, &W);
        // printf("%d %d\n", N, W);

        // car0 pos: (1,1)
        // car1 pos: (N,N)
        D[CAR1][0] = 1;
        D[CAR1][1] = 1;
        D[CAR2][0] = N;
        D[CAR2][1] = N;
        for (int i = 0; i < W; i++) {
            scanf("%d %d", &D[i][0], &D[i][1]);
        }
        // cache init
        for (int i = 0; i < W; i++) {
            for (int j = 0; j < W; j++) {
                C[i][j] = -1;
            }
            C[CAR1][i] = -1;
            C[CAR2][i] = -1;
        }
        int stayCar1 = getDist(CAR2, 0) + minWork(CAR1, 0);
        int stayCar2 = getDist(CAR1, 0) + minWork(CAR2, 0);
        if (stayCar1 < stayCar2) {
            printf("%d\n%d\n", stayCar1, 2);
            int carPos = CAR1;
            int pickCar = 2;
            for (int i = 0; i < W-1; i++) {
                if (Choice[carPos][i] == MOVE_AGAIN) {
                    printf("%d\n", pickCar);
                } else {
                    pickCar = ((pickCar == 1) ? 2 : 1);
                    carPos = i;
                    printf("%d\n", pickCar);
                }
            }
        } else {
            printf("%d\n%d\n", stayCar2, 1);
            int carPos = CAR2;
            int pickCar = 1;
            for (int i = 0; i < W-1; i++) {
                if (Choice[carPos][i] == MOVE_AGAIN) {
                    printf("%d\n", pickCar);
                } else {
                    pickCar = ((pickCar == 1) ? 2 : 1);
                    carPos = i;
                    printf("%d\n", pickCar);
                }
            }
        }

    }

    return 0;
}
