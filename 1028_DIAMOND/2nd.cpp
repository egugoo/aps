#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)<(b)?(b):(a))

#define MAX_RC 751

#define DL 0
#define DR 1

int T, R, C;
int t;
char Data[MAX_RC][MAX_RC];
int Cache[MAX_RC][MAX_RC][2];
int Ans = 0;
int Dir[2][2] = {
    {-1, 1}, {1, 1}
};

int getDist(int y, int x, int dir) {
    if (x < 0 || y < 0 || x >= C || y >= R) return 0;
    int &ret = Cache[y][x][dir];
    if (ret != -1) return ret;
    if (Data[y][x] != '1') return ret = 0;
    ret = getDist(y + Dir[dir][1], x + Dir[dir][0], dir) + 1;
    return ret;
}

int maxDia(int y, int x) {
    if (x < Ans || y > MAX_RC - Ans) return 0;
    int candMax = min(getDist(y, x, DL), getDist(y, x, DR));
    for (int i = candMax; i > 0; i--) {
        if (i <= Ans) return 0;
        int leng = min(getDist(y+i-1, x-i+1, DR), getDist(y+i-1, x+i-1, DL));
        if (leng >= i) return i;
    }

    return 0;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    // scanf("%d", &T);
    T = 1;

    for (t = 0; t < T; t++) {
        scanf("%d %d", &R, &C);

        for (int y = 0; y < R; y++)
            for (int x = 0; x < C; x++) {
                Cache[y][x][DL] = -1;
                Cache[y][x][DR] = -1;
            }

        for (int y = 0; y < R; y++)
            scanf("%s", Data[y]);

        for (int y = 0; y < R; y++)
            for (int x = 0; x < C; x++)
                Ans = max(Ans, maxDia(y, x));
        printf("%d\n", Ans);
    }

    return 0;
}
