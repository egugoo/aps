// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define min(a,b) ((a<b)?a:b)

#define MAX_RC 756

#define UP 0x1
#define DOWN 0x2
#define LEFT 0x4
#define RIGHT 0x8

int T, R, C;
int t;
char Input[MAX_RC];
int Data[MAX_RC][MAX_RC];
int Buf[2][MAX_RC][MAX_RC];

int main()
{
    freopen("input.txt", "rt", stdin);

    // scanf("%d", &T);
    T = 1;

    for (t = 0; t < T; t++) {
        scanf("%d %d", &R, &C);

        int S = (1 + min(R, C))/2;
        int maxSize = 0;

        for (int y = 0; y < R; y++) {
            scanf("%s", Input);
            for (int x = 0; x < C; x++) {
                Data[y][x] = (Input[x] == '0') ? 0 : (UP+DOWN+LEFT+RIGHT);
                Buf[1][y][x] = Data[y][x];
                // printf("%x", Buf[1][y][x]);
                if (Input[x] == '1') maxSize = 1;
            }
            // printf("\n");
        }

        // printf("%d\n", S);

        for (int s = 2; s <= S; s++) {
            int cur = s % 2;
            int bef = (s + 1) % 2;
            // printf("%d %d\n", cur, bef);
            for (int y = s-1; y < R-s+1; y++) {
                for (int x = s-1; x < C-s+1; x++) {
                    Buf[cur][y][x] = 0;
                    if (Data[y][x-s+1] && Data[y][x+s-1]) {
                        if (Buf[bef][y-1][x] & UP) Buf[cur][y][x] += UP;
                        if (Buf[bef][y+1][x] & DOWN) Buf[cur][y][x] += DOWN;
                    }
                    if (Data[y-s+1][x] && Data[y+s-1][x]) {
                        if (Buf[bef][y][x-1] & LEFT) Buf[cur][y][x] += LEFT;
                        if (Buf[bef][y][x+1] & RIGHT) Buf[cur][y][x] += RIGHT;
                    }
                    if (Buf[cur][y][x] == UP+DOWN+LEFT+RIGHT) maxSize = s;
                    // printf("%d", Buf[cur][y][x]);
                }
                // printf("\n");
            }
            // printf("\n");
        }
        printf("%d\n", maxSize);
    }

    return 0;
}
