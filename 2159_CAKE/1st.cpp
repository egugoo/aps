// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define INF 987654321

#define MAX_N 100001
#define INIT (-1)
#define min(a,b) ((a<b)?a:b)
#define max(a,b) ((a<b)?b:a)
#define abs(a) (((a)<0)?(-(a)):(a))

int T, N;
int t;
long D[MAX_N][2];
long Cache[MAX_N][5];

int Dir[5][2] = {{0,0}, {-1,0}, {0,-1}, {1,0}, {0,1}};

long minRoad(int n, int idx) {
    if (N == n) return 0;

    long x = D[n][0] + Dir[idx][0];
    long y = D[n][1] + Dir[idx][1];
    if (x < 0 || y < 0 || MAX_N < x || MAX_N < y) return INF;

    long &ret = Cache[n][idx];
    if (ret != -1) return ret;
    ret = INF;
    for (int i = 0; i < 5; i++) {
        long nx = D[n+1][0] + Dir[i][0];
        long ny = D[n+1][1] + Dir[i][1];
        ret = min(ret, abs(nx-x) + abs(ny-y) + minRoad(n+1, i));
    }
    return ret;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    // scanf("%d", &T);
    T = 1;

    for (t = 0; t < T; t++) {
        scanf("%d", &N);

        for (int i = 0; i < N+1; i++) {
            scanf("%ld %ld", &D[i][0], &D[i][1]);
            Cache[i][0] = -1;
            Cache[i][1] = -1;
            Cache[i][2] = -1;
            Cache[i][3] = -1;
            Cache[i][4] = -1;
        }

        printf("%ld\n", minRoad(0, 0));
    }

    return 0;
}
