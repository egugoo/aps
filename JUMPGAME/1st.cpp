// JUMPGAME

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define MAX_N 100

int T, N;
int t;
int board[MAX_N][MAX_N];
int cache[MAX_N][MAX_N];

int jump(int y, int x) {
    if (y >= N || x >= N) return 0;
    if (x == N-1 && y == N-1) return 1;

    int &ret = cache[y][x];
    if (ret != -1) return ret;

    int jumpSize = board[y][x];
    ret = jump(y+jumpSize, x) || jump(y, x+jumpSize);
    return ret;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d", &N);
        for (int y = 0; y < N; y++)
            for (int x = 0; x < N; x++) {
                scanf("%d", &board[y][x]);
                cache[y][x] = -1;
            }

        if (jump(0,0) == 1)
            printf("YES\n");
        else
            printf("NO\n");
    }

    return 0;
}
