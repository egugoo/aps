#include <stdio.h>

#define MAX_XY 20
#define MAX_B 150
#define X 0
#define Y 1
#define RT 0
#define DN 1
#define LT 2
#define UP 3
#define min(a,b) (a<b?a:b)

int T, t, N;
char D[MAX_XY][MAX_XY+1];
int Cache[MAX_B][4];
int B[MAX_B+2][2];
int Dir[4][2] = {
    { 1,  0},  // Right
    { 0,  1},  // Down
    {-1,  0},  // Left
    { 0, -1}   // Up
};
bool Visited[MAX_XY][MAX_XY][4];
struct Q {
    int x;
    int y;
    int dir;
    int cnt;
};
Q Buf[MAX_XY*MAX_XY*4];
int qstart = 0;
int qend = 0;

void enq(int y, int x, int dir, int cnt) {
    Buf[qend].x = x;
    Buf[qend].y = y;
    Buf[qend].dir = dir;
    Buf[qend].cnt = cnt;
    qend++;
}

Q deq() {
    return Buf[qstart++];
}

bool isEmpty() {
    return qstart == qend;
}

void clearQ() {
    qstart = 0;
    qend = 0;
}

void printQ() {
    for (int i = qstart; i<qend; i++) {
        printf("%d: %d %d %d %d\n", i, Buf[i].x,Buf[i].y,Buf[i].dir,Buf[i].cnt);
    }
}

void initVisited() {
    for (int y = 0; y < MAX_XY; y++)
        for (int x = 0; x < MAX_XY; x++)
            for (int d = 0; d < 4; d++)
                Visited[y][x][d] = false;
}

bool isVisited(int y, int x, int dir) {
    return Visited[y][x][dir];
}

void enqLine(int y, int x, int dir, int cnt) {
    x += Dir[dir][X];
    y += Dir[dir][Y];
    while (
        0 <= x && 0 <= y
        && x < MAX_XY && y < MAX_XY
        && !Visited[y][x][dir]
        && D[y][x] != '*')
    {
        Visited[y][x][dir] = true;
        enq(y, x, dir, cnt);
        x += Dir[dir][X];
        y += Dir[dir][Y];
    }
}

int goNext(int n, int dir, int eDir) {
    int x = B[n][X];
    int y = B[n][Y];
    int ret = 987654321;

    if (B[n][X] == B[n+1][X] && B[n][Y] == B[n+1][Y] && dir == eDir) {
        return 0;
    }

    // init enqueue
    enqLine(y, x, dir, 0);  // straight
    enqLine(y, x, (dir+1)%4, 1);  // 1 clock-wise turn

    // printQ();

    while (!isEmpty()) {
        Q q = deq();
        if (q.x == B[n+1][X] && q.y == B[n+1][Y] && q.dir == eDir) {
            clearQ();
            ret = q.cnt;
        } else {
            enqLine(q.y, q.x, (q.dir + 1) % 4, q.cnt+1);
        }
    }
    clearQ();
    return ret;
}
int minTurn(int n, int dir) {
    if (n == N-1) return 0;
    int &ret = Cache[n][dir];
    if (ret != -1) return ret;

    ret = 987654321;

    for (int i = 0; i < 4; i++) {
        initVisited();
        int cost = goNext(n, dir, i);
        if (cost == 987654321) continue;
        // printf("n%d s%d d%d c%d\n", n, dir, i, cost);
        ret = min(ret, cost + minTurn(n+1, i));
    }

    return ret;
}

int main(void) {
    freopen("input.txt", "rt", stdin);
    scanf("%d", &T);
    while (T--) {
        // start point
        N = 1;
        B[0][X] = 0;
        B[0][Y] = 0;

        for (int y=0; y < MAX_XY; y++) {
            scanf("%s", D[y]);
            for (int x=0; x < MAX_XY; x++) {
                if (D[y][x] != '.' && D[y][x] != '*') {
                    N++;
                    B[(D[y][x] - 'a') + 1][X] = x;
                    B[(D[y][x] - 'a') + 1][Y] = y;
                    D[y][x] = '.';
                }
            }
        }
        // end point
        B[N][X] = 19;
        B[N][Y] = 19;
        N++;

        // init cache
        for (int i = 0; i < N; i++)
            for (int j = 0; j < 4; j++)
                Cache[i][j] = -1;

        // for (int i = 0; i < N; i++) {
        //     printf("%c: %d %d\n", i + 'a' - 1, B[i][X], B[i][Y]);
        // }
        // printf("go: %d\n", goNext(3, UP, RT));

        // printf("N: %d\n", N);

        printf("%d\n", minTurn(0, RT));
        // // printf("%d\n", goNext(0, RT, DN));

        // printf("\nCache\n");
        // for (int i = 0; i < N; i++) {
        //     printf("%c: ", i+'a'-1);
        //     for (int j = 0; j < 4; j++) {
        //         printf("%d\t", Cache[i][j]);
        //     }
        //     printf("\n");
        // }
        // printf("\n");
    }
    return 0;
}
