#include <stdio.h>

#define MAX_C 300
#define MAX_T 20
#define MAX_N 20

#define max(a,b) ((a<b)?b:a)

int T, t, C, N;
int D[MAX_C+1][MAX_N];
int Cache[MAX_C+1][MAX_N][2];

int maxInvest(int c, int n) {
    if (c == 0) return 0;
    if (c < 0) return -987654321;
    if (n >= N) return 0;

    int &ret = Cache[c][n][0];
    if (ret != -1) return ret;

    for (int i = 0; i <= c; i++) {
        int candi = maxInvest(c - i, n + 1) + D[i][n];
        if (ret < candi) {
            ret = candi;
            Cache[c][n][1] = i;
        }
    }

    return ret;
}

int main() {
    freopen("input.txt", "rt", stdin);
    scanf("%d", &T);
    // T = 1;
    for (int t = 0; t < T; t++) {
        scanf("%d %d", &C, &N);
        for (int j = 0; j < N; j++) {
            D[0][j] = 0;
            Cache[0][j][1] = 0;
        }
        for (int i = 1; i <= C; i++) {
            scanf("%d", &D[i][0]);
            for (int j = 0; j < N; j++) {
                scanf("%d", &D[i][j]);
                Cache[i][j][0] = -1;
            }
        }

        printf("%d\n", maxInvest(C, 0));
        int c = C;
        for (int n = 0; n < N; n++) {
            printf("%d ", Cache[c][n][1]);
            c -= Cache[c][n][1];
        }
        printf("\n");
    }
    return 0;
}
