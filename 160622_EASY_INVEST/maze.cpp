#include <stdio.h>

int money;
int companyCount;
int profits[20][301];
int history[20];
int cache[20][301][2];

int solve(int n, int curMoney)
{
    if(curMoney == 0) return 0;
    if(curMoney < 0) return -987654321;
    if(n >= companyCount) return 0;
    if(cache[n][curMoney][0] != -1) return cache[n][curMoney][0];

    int curAnswer = 0;
    for(int i = 0; i <= curMoney; i++) {
        int ret = profits[n][i] + solve(n+1, curMoney - i);
        if(ret > curAnswer) {
            curAnswer = ret;
            cache[n][curMoney][1] = i;
        }
    }

    cache[n][curMoney][0] = curAnswer;
    return curAnswer;
}

int kka(int n, int curMoney)
{
    if(n >= companyCount) return 0;

    int value = cache[n][curMoney][0];
    history[n] = cache[n][curMoney][1];
    kka(n+1, curMoney - cache[n][curMoney][1]);
    return 0;
}

int main()
{
    int temp;
    // freopen("input.txt", "rt", stdin);

    scanf("%d %d", &money, &companyCount);
    for(int i = 1; i <= money; i++) {
        scanf("%d", &temp);
        for(int j = 0; j < companyCount; j++) {
            scanf("%d", &profits[j][i]);
        }
    }

    for(int i = 0; i < 20; i++) for(int j = 0; j < 301; j++) { cache[i][j][0] = cache[i][j][1] = -1; }
    for(int i = 0; i < 20; i++) { cache[i][0][1] = 0;}

    printf("%d\n", solve(0, money));

    // kka(0, money);

    // for(int i = 0; i < companyCount; i++) {
    //     printf("%d ", history[i]);
    // }
    // printf("\n");
    int c = money;
    for (int n = 0; n < companyCount; n++) {
        printf("%d ", cache[n][c][1]);
        c -= cache[n][c][1];
    }
    printf("\n");

    return 0;
}
