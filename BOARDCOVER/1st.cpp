// BOARDCOVER
// 1st

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

int C, H, W;

int nextPosition(int cur, char board[21][21]) {
    // TODO: error handling
    while (cur < H*W && board[cur / W][cur % W] != '.')
        cur++;
    return cur;
}

void printBlock(char board[21][21]) {
    printf("\n");
    for (int j = 0; j < H; ++j) {
        for (int k = 0; k < W; ++k) {
            printf("%c", board[j][k]);
        }
        printf("\n");
    }
}


// TODO: describe blocks
// |_, _|, |-, -|
int dx[4][3] = {{0, 0, 1}, {0, -1, 0}, {0, 1, 0}, {0, -1, 0}};
int dy[4][3] = {{0, 1, 1}, {0,  1, 1}, {0, 0, 1}, {0,  0, 1}};

bool canBlockCover(int blockId, int cur, char board[21][21]) {
    int x = cur % W;
    int y = cur / W;
    for (int i = 0; i < 3; i++) {
        int x1 = x + dx[blockId][i];
        int y1 = y + dy[blockId][i];
        if (x1 < 0 || W < x1) return false;
        if (y1 > H) return false;
        if (board[x1][y1] != '.') return false;
        printf(".");
    }
    printf("\n");
    return true;
}

void coverBlock(int blockId, int cur, char board[21][21]) {
    int x = cur % W;
    int y = cur / W;
    for (int i = 0; i < 3; i++) {
        int x1 = x + dx[blockId][i];
        int y1 = y + dy[blockId][i];
        board[x1][y1] = '#';
    }
}

int countCover(int cur, int remain, char board[21][21]) {
    int count = 0;
    for (int i = 0; i < 4; ++i) {
        if (canBlockCover(i, cur, board)) {
            printf("c");
            if (remain == 3) {
                return 1;
            }
            else {
                coverBlock(i, cur, board);
                count += countCover(cur, remain - 3, board);
                cur = nextPosition(cur, board);
            }
        }
        else {
            printf("! blockId[%d], x: %d, y: %d\n", i, cur%W, cur/W);
        }
    }
    return count;
}

int main()
{
    char board[21][21];
    freopen("input.txt", "rt", stdin);

    scanf("%d", &C);

    for (int i = 0; i < C; ++i) {
        scanf("%d %d", &H, &W);

        for (int j = 0; j < H; ++j)
            scanf("%s", board[j]);

        // find first '.'
        int first = nextPosition(0, board);

        int k = 0;
        int remain = 0;
        while (k++ < H*W)
            if (board[k / W][k % W] == '.') remain++;

        // TODO: cut no '.'
        // TODO: cut not 3 x '.'

        // count!!
        printf("%d %d ", first/W, first%W);
        printf("%d\n", countCover(first, remain, board));
    }

    return 0;
}
