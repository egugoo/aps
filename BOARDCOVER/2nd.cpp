// BOARDCOVER
// 2nd
// 2016. 05. 17

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

int t, C, H, W;
char tmpBoard[21][21];
int board[21][21];

int coverType[4][3][2] = {
    {{0,0}, {1,0}, {0,1}},
    {{0,0}, {0,1}, {1,1}},
    {{0,0}, {1,0}, {1,1}},
    {{0,0}, {1,-1}, {1,0}}
};

void printBoard() {
    for (int y = 0; y < H; ++y) {
        for (int x = 0; x < W; ++x) {
            printf("%d", board[y][x]);
        }
        printf("\n");
    }
    printf("\n");
}

bool set(int y, int x, int type, int delta) {
    bool ok = true;
    for (int i = 0; i < 3; ++i) {
        int ny = y + coverType[type][i][0];
        int nx = x + coverType[type][i][1];
        if (ny < 0 || H <= ny || nx < 0 || W <= nx) {
            ok = false;
        }
        else {
            if ((board[ny][nx] += delta) > 1)
                ok = false;
        }
    }
    return ok;
}

int countCover() {
    // find first '.'
    int y = -1;
    int x = -1;
    for (int i = 0; i < H; ++i) {
        for (int j = 0; j < W; ++j) {
            if (board[i][j] == 0) {
                x = j;
                y = i;
                break;
            }
        }
        if (y != -1) break;
    }

    // base case
    if (y == -1) {
        return 1;
    }
    int ret = 0;

    for (int type=0; type<4; ++type) {
        if (set(y,x,type,1))
            ret += countCover();
        set(y,x,type,-1);
    }

    return ret;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &C);

    for (int t = 0; t < C; ++t) {
        scanf("%d %d", &H, &W);

        for (int i = 0; i < H; ++i) {
            scanf("%s", tmpBoard[i]);
        }

        for (int y = 0; y < H; ++y)
            for (int x = 0; x < W; ++x)
                if (tmpBoard[y][x] == '#')
                    board[y][x] = 1;
                else
                    board[y][x] = 0;

        int remain = 0;
        for (int y = 0; y < H; ++y)
            for (int x = 0; x < W; ++x)
                if (board[y][x] == 0) remain++;

        if (remain == 0 || remain % 3 != 0) {
            printf("0\n");
            continue;
        }

        // count!!
        printf("%d\n", countCover());
    }

    return 0;
}
