// FAIL

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define MAX_S 10000 + 1
#define MAX_N 100

int T, N, S;
int t, z;
int MinBad = 987654321;
int MaxBad = -987654321;

int Bad[MAX_N];
int Road[MAX_S];

#define min(a,b) ((a<b)?a:b)
#define max(a,b) ((a<b)?b:a)

void qsort(int lo, int hi)
{
    int p = Bad[(lo + hi) / 2];
    int l = lo;
    int r = hi;
    while (l < r) {
        while (Bad[l] < p) l++;
        while (Bad[r] > p) r--;
        if (l <= r) {
            int t = Bad[l];
            Bad[l] = Bad[r];
            Bad[r] = t;
            l++;
            r--;
        }
    }
    if (lo < r) qsort(lo, r);
    if (l < hi) qsort(l, hi);
}

int countCovered() {
    z = 0;
    int c = 0;
    while (z < MAX_S) {
        if (Road[z++] > 0) c++;
    }
    return c;
}

int minCovered(int n) {
    if (n >= N) return countCovered();
    int ret = 987654321;

    // cover each -S ~ 0
    for (int i = -S + 1 + Bad[n]; i <= Bad[n]; ++i) {
        if (i < 0) continue;
        // cover
        for (int j = 0; j < S; ++j) {
            Road[i + j] += 2;
        }
        ret = min(ret, minCovered(n+1));
        // rollback
        for (int j = 0; j < S; ++j) {
            Road[i + j] -= 2;
        }
    }
    return ret;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d %d", &N, &S);
        printf("%d %d\n", N, S);

        // input Bad, get Max
        MinBad = 987654321;
        MaxBad = -987654321;
        z = 0;
        while (z < N) {
            scanf("%d", &Bad[z++]);
            // MinBad = min(MinBad, Bad[z-1]);
            // MaxBad = max(MaxBad, Bad[z-1]);
        }

        // sort Bad
        qsort(0, N-1);

        // init ROAD
        z = 0;
        while (z < MAX_S)
            Road[z++] = 0;

        printf("%d\n", minCovered(0));
    }

    return 0;
}
