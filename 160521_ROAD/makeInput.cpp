#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define MAX_S 10000 + 1
#define MAX_N 100

int T, N, S;
int t, z;
int MinBad = 987654321;
int MaxBad = -987654321;

int Bad[MAX_N];
int Road[MAX_S];

#define min(a,b) ((a<b)?a:b)
#define max(a,b) ((a<b)?b:a)
#define abs(a) ((a<0)?(-a):a)

void qsort(int lo, int hi)
{
    int p = Bad[(lo + hi) / 2];
    int l = lo;
    int r = hi;
    while (l < r) {
        while (Bad[l] < p) l++;
        while (Bad[r] > p) r--;
        if (l <= r) {
            int t = Bad[l];
            Bad[l] = Bad[r];
            Bad[r] = t;
            l++;
            r--;
        }
    }
    if (lo < r) qsort(lo, r);
    if (l < hi) qsort(l, hi);
}

int minCovered(int start, int n) {
    int ret = 987654321;

    if (N <= n) {
        ret = 0;
    } else {
        for (int i = 0; i < S; ++i) {
            int coverStart = Bad[n] - i;
            int coverEnd = Bad[n] - i + S;
            coverEnd = max(start, coverEnd);
            // printf("%d %d %d %d\n", n, Bad[n], i, coverEnd);
            if (Bad[n] - i < 0 || MAX_S < coverEnd) continue;
            int covered = S;
            if (coverStart < start) covered = start - coverStart;
            ret = min(ret, minCovered(coverEnd, n+1) + covered);
        }
    }

    return ret;
}

int main()
{
    freopen("input.txt", "wt", stdout);

    T = 2;

    printf("%d\n", T);

    for (t = 0; t < T; t++) {
        N = abs(rand() % 10);
        S = abs(rand() % 10000);
        printf("%d %d\n", N, S);

        z = 0;
        while (z++ < N) {
            printf("%d ", abs(rand() % 10000));
        }
        printf("\n");
    }

    return 0;
}
