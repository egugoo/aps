#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define MAX_S 10000 + 1
#define MAX_N 100

int T, N, S;
int t, z;
int MinBad = 987654321;
int MaxBad = -987654321;

int Bad[MAX_N];
int Road[MAX_S];

int Cache[MAX_S][MAX_N];

#define min(a,b) ((a<b)?a:b)
#define max(a,b) ((a<b)?b:a)

void qsort(int lo, int hi)
{
    int p = Bad[(lo + hi) / 2];
    int l = lo;
    int r = hi;
    while (l < r) {
        while (Bad[l] < p) l++;
        while (Bad[r] > p) r--;
        if (l <= r) {
            int t = Bad[l];
            Bad[l] = Bad[r];
            Bad[r] = t;
            l++;
            r--;
        }
    }
    if (lo < r) qsort(lo, r);
    if (l < hi) qsort(l, hi);
}

int minCovered(int start, int n) {
    int &ret = Cache[start][n];

    if (ret != 987654321) return ret;

    if (N <= n) {
        ret = 0;
    } else {
        for (int i = 0; i < S; ++i) {
            int coverStart = Bad[n] - i;
            int coverEnd = Bad[n] - i + S;
            coverEnd = max(start, coverEnd);
            if (Bad[n] - i < 0 || MAX_S < coverEnd) continue;
            int covered = S;
            if (coverStart < start) covered = start - coverStart;
            ret = min(ret, minCovered(coverEnd, n+1) + covered);
        }
    }

    return ret;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d %d", &N, &S);
        printf("%d %d\n", N, S);

        // input Bad, get Max
        MinBad = 987654321;
        MaxBad = -987654321;
        z = 0;
        while (z < N) {
            scanf("%d", &Bad[z++]);
            // MinBad = min(MinBad, Bad[z-1]);
            // MaxBad = max(MaxBad, Bad[z-1]);
        }

        for (int i = 0; i < N; i++)
            for (int j = 0; j < MAX_S; j++)
                Cache[j][i] = 987654321;

        // sort Bad
        qsort(0, N-1);

        // init ROAD
        z = 0;
        while (z < MAX_S)
            Road[z++] = 0;

        printf("%d\n", minCovered(0, 0));
    }

    return 0;
}
