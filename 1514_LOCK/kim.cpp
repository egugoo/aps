#include <iostream>

#define MAX_N (100)
#define INF (987654321)

#define abs(a) (a < 0 ? -a : a)

using namespace std;

int N, T;
int cache[MAX_N][10][10];
int Lock[MAX_N + 2];
int password[MAX_N];

int solution(int n, int next1, int next2)
{
    int& ret = cache[n][next1][next2];
    if (ret != INF)
        return ret;

    int up = password[n] - next1;
    if (up < 0) up += 10;
    int down = next1 - password[n];
    if (down < 0) down += 10;

    int diff = up < down ? up : down;

    if (n == N - 1)
    {
        return ((diff + 2) / 3);
    }

    int next3 = Lock[n + 2];

    if (diff == 0)
    {
        ret = solution(n + 1, next2, next3);
        return ret;
    }

    int ret2;

    for (int i = -5; i < 5; i++) // 1개만 돌리는 경우
    {
        for (int j = -5; j < 5; j++) // 2개 돌리는 경우
        {
            if (n < N - 2)
            {
                for (int k = -5; k < 5; k++) // 3개 돌리는 경우
                {
                    if (i != 0 || j != 0 || k != 0)
                    {
                        int newNext1 = next1 + i + j + k;
                        while (newNext1 < 0) newNext1 += 10;
                        while (newNext1 > 9) newNext1 -= 10;
                        if (newNext1 == password[n]) // 3x3x3 의 경우 중 password를 맞춘 경우만 체크
                        {
                            int cost = (abs(i) + 2) / 3 + (abs(j) + 2) / 3 + (abs(k) + 2) / 3;
                            int newNext2 = next2 + j + k;
                            while (newNext2 < 0) newNext2 += 10;
                            while (newNext2 > 9) newNext2 -= 10;
                            int newNext3 = next3 + k;
                            while (newNext3 < 0) newNext3 += 10;
                            while (newNext3 > 9) newNext3 -= 10;
                            ret2 = solution(n + 1, newNext2, newNext3) + cost;
                            if (ret2 < ret) ret = ret2;
                        }
                    }
                }
            }
            else
            {
                if (i != 0 || j != 0)
                {
                    int newNext1 = next1 + i + j;
                    while (newNext1 < 0) newNext1 += 10;
                    while (newNext1 > 9) newNext1 -= 10;
                    if (newNext1 == password[n]) // 3x3x3 의 경우 중 password를 맞춘 경우만 체크
                    {
                        int cost = (abs(i) + 2) / 3 + (abs(j) + 2) / 3;
                        int newNext2 = next2 + j;
                        while (newNext2 < 0) newNext2 += 10;
                        while (newNext2 > 9) newNext2 -= 10;
                        ret2 = solution(n + 1, newNext2, next3) + cost;
                        if (ret2 < ret) ret = ret2;
                    }
                }
            }
        }
    }

    return ret;
}

void strToIntArr(char input[], int output[])
{
    for (int i = 0; i < N; i++)
    {
        output[i] = input[i] - '0';
    }
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);
    // T = 1;

    while (T--) {

    char input1[MAX_N + 1];
    char input2[MAX_N + 1];

    cin >> N;
    cin >> input1;
    strToIntArr(input1, Lock);
    cin >> input2;
    strToIntArr(input2, password);

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < 10; j++)
            for (int k = 0; k < 10; k++)
                cache[i][j][k] = INF;
    }

    cout << solution(0, Lock[0], Lock[1]) << " ";

    if (T % 4 == 0) printf("\n");
    }

    return 0;
}
