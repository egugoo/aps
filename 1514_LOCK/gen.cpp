#include <stdio.h>
#include <stdlib.h>

#define MAX_N 100

char input1[MAX_N+1];
char input2[MAX_N+1];
char input3[MAX_N+1];
char input4[MAX_N+1];
int T = 10;

int main() {
    freopen("input.txt", "wt", stdout);
    srand(0);
    printf("%d\n", T*4);
    for (int t = 0; t < T*4; t++) {
        for (int i=0; i < MAX_N; i++) {
            input1[i] = rand()%10 + '0';
            input2[i] = rand()%10 + '0';
            input3[MAX_N-i-1] = input1[i];
            input4[MAX_N-i-1] = input2[i];
        }
        input1[MAX_N] = 0;
        input2[MAX_N] = 0;
        input3[MAX_N] = 0;
        input4[MAX_N] = 0;
        printf("%d\n%s\n%s\n", MAX_N, input1, input2);
        printf("%d\n%s\n%s\n", MAX_N, input2, input1);
        printf("%d\n%s\n%s\n", MAX_N, input3, input4);
        printf("%d\n%s\n%s\n", MAX_N, input4, input3);
    }
    return 0;
}
