#include <stdio.h>

#define MAX_N (100)
#define min(a,b) (((a)<(b))?(a):(b))
#define m10(a) ((a+10)%10)

int T, N;
char Input[2][MAX_N+1];
int D[2][MAX_N+2];
int Cache[MAX_N+2][10][10];
int Cost[10] = {0,1,1,1,2,2,2,1,1,1};

int minWay(int n, int d0, int d1) {
    if (n == N) return 0;

    int &ret = Cache[n][d0][d1];
    if (ret != 987654321) return ret;

    if (d0 == D[1][n]) return ret = minWay(n+1, d1, D[0][n+2]);

    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            int k = m10(D[1][n] - m10(d0+i+j));
            ret = min(
                ret,
                minWay(n+1, m10(d1+j+k), m10(D[0][n+2]+k))
                + Cost[i] + Cost[j] + Cost[k]
            );
        }
    }
    return ret;
}

int main() {
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);
    // T = 1;

    while (T--) {
        scanf("%d %s %s", &N, Input[0], Input[1]);

        for (int i = 0; i < N; i++) {
            D[0][i] = Input[0][i] - '0';
            D[1][i] = Input[1][i] - '0';
        }
        for (int i = 0; i < N+2; i++)
            for (int j = 0; j < 10; j++)
                for (int k = 0; k < 10; k++)
                    Cache[i][j][k] = 987654321;
        D[0][N] = 0;
        D[0][N+1] = 0;
        D[1][N] = 0;
        D[1][N+1] = 0;
        printf("%d ", minWay(0, D[0][0], D[0][1]));

        if (T % 4 == 0) printf("\n");
    }

    return 0;
}
