#include <stdio.h>

int T, N;
char D[2][101];
int Delta[100];
int Cache[1000][1000];
int Answer = 987654321;

int minWay(int in, int out) {
    int &ret = Cache[in][out];
    if (ret != -1) return ret;
    ret = 1;
    return ret;
}

int main() {
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (int i = 0; i < 1000; i++)
        for (int j = 0; j < 1000; j++)
            Cache[i][j] = -1;

    Cache[0][0] = 0;
    Cache[1][0] = 1;
    Cache[2][0] = 1;
    Cache[3][0] = 1;
    Cache[4][0] = 2;
    Cache[5][0] = 2;
    Cache[6][0] = 2;
    Cache[7][0] = 1;
    Cache[8][0] = 1;
    Cache[9][0] = 1;

    while (T--) {
        scanf("%d %s %s", &N, D[0], D[1]);
        for (int i = 0; i < N; i++) {
            Delta[i] = ((D[1][i] - D[0][i]) + 10) % 10;
            printf("%d", Delta[i]);
        }
        printf("\n");

        Answer = 987654321;
        if (N < 4) {
            int in = Delta[0];
            for (int i = 1; i < N; i++) {
                in *= 10;
                in += Delta[i];
            }
            Answer = minWay(in, 0);
            printf("%d\n\n", Answer);
        }
        // for (int i = 0; i < N-2; i++) {
        //     minWay()
        // }
    }
    return 0;
}
