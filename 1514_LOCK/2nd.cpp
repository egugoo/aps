#include <stdio.h>

int T, N;
char Input[2][101];
int D[104];
int Cache[104][10][10][10];

#define abs(a) (((a)<0)?(-(a)):(a))
#define min(a,b) (((a)<(b))?(a):(b))

int minWay(int n, int d1, int d2, int d3) {
    if (n >= N) return 0;
    d1 = (d1 + 10) % 10;
    d2 = (d2 + 10) % 10;
    d3 = (d3 + 10) % 10;

    int &ret = Cache[n][d1][d2][d3];
    if (ret != -1) return ret;

    if ((D[n] + d1) % 10 == 0) {
        if ((D[n+1] + d2) % 10 == 0) {
            if ((D[n+2] + d3) % 10 == 0) {
                return ret = minWay(n+3, 0, 0, 0);
            }
            return ret = minWay(n+2, d3, 0, 0);
        }
        return ret = minWay(n+1, d2, d3, 0);
    }

    ret = 987654321;

    for (int i = -5; i < 5; ++i) {
        ret = min(ret, minWay(n, d1+i, d2+0, d3+0) + ((abs(i) + 2) / 3));
        ret = min(ret, minWay(n, d1+i, d2+i, d3+0) + ((abs(i) + 2) / 3));
        ret = min(ret, minWay(n, d1+i, d2+i, d3+i) + ((abs(i) + 2) / 3));
    }
    return ret;
}

int main() {
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);
    // T = 1;

    while (T--) {
        for (int i = 0; i < 104; i++)
            for (int j = 0; j < 10; j++)
                for (int k = 0; k < 10; k++)
                    for (int l = 0; l < 10; l++)
                        Cache[i][j][k][l] = -1;

        scanf("%d %s %s", &N, Input[0], Input[1]);
        for (int i = 0; i < N; i++) {
            D[i] = (Input[1][i] - Input[0][i] + 10) % 10;
        }
        D[N] = 0;
        D[N+1] = 0;
        D[N+2] = 0;
        D[N+3] = 0;
        printf("%d\n", minWay(0, 0, 0, 0));
        if (T % 4 == 0) printf("\n");
    }
    return 0;
}
