#include <stdio.h>

int T, N;
char Input[2][101];
int D[2][102];
int Cache[102][10][10];
int Cost[10] = {0,1,1,1,2,2,2,1,1,1};

#define abs(a) (((a)<0)?(-(a)):(a))
#define min(a,b) (((a)<(b))?(a):(b))
#define m10(a) ((a+10)%10)

int cost(int a, int b) {
    return Cost[ m10(b - a) ];
}

// int minWay(int n, int d0, int d1) {
//     if (n >= N) return 0;

//     int &ret = Cache[n][d0][d1];
//     if (ret != -1) return ret;

//     ret = 987654321;
//     ret = min( ret, minWay(n+1, d1, D[0][n+2]) + cost(d0, D[1][n]));  // n
//     for (int i = 0; i < 10; ++i) {
//         int need = cost( m10(d0+i), D[1][n] );
//         ret = min( ret, minWay(n+1, m10(d1+i), D[0][n+1]) + Cost[m10(i)] + need);  // n, n+1
//         ret = min( ret, minWay(n+1, m10(d1+i), m10(D[0][n+1]+i)) + Cost[m10(i)] + need);  // n, n+1, n+2
//     }
//     return ret;
// }

// int minWay(int n, int d0, int d1) {
//     if (n >= N) return 0;

//     int &ret = Cache[n][d0][d1];
//     if (ret != -1) return ret;

//     ret = 987654321;
//     ret = min( ret, minWay(n+1, d1, D[0][n+1]) + cost(d0, D[1][n]));  // n
//     for (int i = 0; i < 10; ++i) {
//         int need = cost( m10(d0+i), D[1][n] );
//         ret = min( ret, minWay(n+1, m10(d1+i), m10(D[0][n+1]+i)) + Cost[i] + need);  // n, n+1, n+2
//         for (int j = 0; j < 10; ++j) {
//             int need2 = cost( m10(d0+i+j), D[1][n] );
//             ret = min( ret, minWay(n+1, m10(d1+i+j), m10(D[0][n+1]+i)) + Cost[i] + Cost[j] + need2);  // n, n+1
//         }
//     }
//     return ret;
// }

int minWay(int n, int d0, int d1) {
    if (n >= N) return 0;

    int &ret = Cache[n][d0][d1];
    if (ret != -1) return ret;

    ret = 987654321;
    ret = min( ret, minWay(n+1, d1, D[0][n+2]) + cost(d0, D[1][n]) );  // n
    for (int i = 1; i < 10; ++i) {
        ret = min( ret, minWay(n+1, m10(d1+i), D[0][n+2]) + Cost[i] + cost(m10(d0+i), D[1][n]) );  // n, n+1
        ret = min( ret, minWay(n+1, m10(d1+i), m10(D[0][n+2]+i)) + Cost[i] + cost(m10(d0+i), D[1][n]) );  // n, n+1, n+2
    }
    return ret;
}

int main() {
    // freopen("input.txt", "rt", stdin);

    scanf("%d", &T);
    // T = 1;

    while (T--) {
        for (int i = 0; i < 102; i++)
            for (int j = 0; j < 10; j++)
                for (int k = 0; k < 10; k++)
                    Cache[i][j][k] = -1;

        scanf("%d %s %s", &N, Input[0], Input[1]);
        for (int i = 0; i < N; i++) {
            D[0][i] = Input[0][i] - '0';
            D[1][i] = Input[1][i] - '0';
        }
        D[0][N] = 0;
        D[0][N+1] = 0;
        D[1][N] = 0;
        D[1][N+1] = 0;
        printf("%d\n", minWay(0, D[0][0], D[0][1]));

        if (T % 4 == 0) printf("\n");
    }
    return 0;
}
