#include <iostream>
#include <stdio.h>

#define MAX_T 50
#define MAX_L 201
#define max(a,b) ((a>b)?a:b)

int T, L0, L1;
int t;
char D[2][MAX_L];

int len(char *str) {
    int c = 0;
    while (*(str++) != 0) c++;
    return c;
}

#define D_WAS_NONE -1
#define D_WAS_0 0
#define D_WAS_1 1

int maxScore(int cur0, int cur1, int d_was) {
    if (D[0][cur0] == 0) {
        if (d_was == D_WAS_0) return -1 * (L1 - cur1);
        if (L1 - cur1 < 2) return -4 * (L1 - cur1);
        else return -3 + (-1 * (L1 - cur1 - 1));
    } else if (D[1][cur1] == 0) {
        if (d_was == D_WAS_1) return -1 * (L0 - cur0);
        if (L0 - cur0 < 2) return -4 * (L0 - cur0);
        else return -3 + (-1 * (L0 - cur0 - 1));
    }

    // 3 cases
    // case #1: same or diff
    int case1 = maxScore(cur0+1, cur1+1, D_WAS_NONE);
    if (D[0][cur0] == D[1][cur1]) {
        case1 += 1; // +1
    } else {
        case1 += -4; // -4
    }

    // case #2: insert D to 0
    int case2 = maxScore(cur0, cur1+1, D_WAS_0);
    if (d_was == D_WAS_NONE || d_was == D_WAS_1) {
        case2 += -3; // -3
    } else { // D_WAS_0
        case2 += -1; // -1
    }

    // case #3: insert D to 1
    int case3 = maxScore(cur0+1, cur1, D_WAS_1);
    if (d_was == D_WAS_NONE || d_was == D_WAS_0) {
        case3 += -3; // -3
    } else { // D_WAS_1
        case3 += -1; // -1
    }

    int max = max( max(case1, case2), case3 );
    // printf("%d %d, %d %d %d, (%d)\n", cur0, cur1, case1, case2, case3, max);

    return max;
}


int main() {
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%s", D[0]);
        scanf("%s", D[1]);
        L0 = len(D[0]);
        L1 = len(D[1]);

        printf("%s %d\n", D[0], L0);
        printf("%s %d\n", D[1], L1);
        printf("%d\n", maxScore(0, 0, D_WAS_NONE));
    }

    return 0;
}
