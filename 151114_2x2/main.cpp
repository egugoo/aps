#include <stdio.h>

int TC, T, W, H;
int D[10];
int Max = 0;
#define max(a,b) (a<b?b:a)
#define row(x) (x/W)
#define col(x) (x%W)

int top = 0;
int Stack[99999999];
void push(int x) {
    Stack[top++] = x;
}
int pop() {
    return Stack[--top];
}

void findAnswer(int count, int w, int h) {
    if (h >= H) return;
    if (!((D[h] & (1 << w)) >> w)) {
        // fill
        // printf("%d, %d\n", w, h);
        push(D[h]);
        if (w > 0)
            D[h] |= (7 << w - 1);
        else
            D[h] |= 3;
        if (h+1 < H) {
            push(D[h+1]);
            if (w > 0)
                D[h+1] |= (7 << w - 1);
            else
                D[h+1] |= 3;
        }
        // if (Max < count+1) {
        //     // debug
        //     for (int h = 0; h < H; h++) {
        //         for (int w = 0; w < W; w++) {
        //             printf("%d ", (D[h] & (1 << w)) >> w);
        //         }
        //         printf("\n");
        //     }
        //     printf("@ %d\n", count+1);
        // }

        Max = max(Max, count+1);

        findAnswer(count+1, (w+1)%W, h+((w+1)/W));

        if (h+1 < H) D[h+1] = pop();
        D[h] = pop();
    }
    findAnswer(count, (w+1)%W, h+((w+1)/W));
    return;
}

int main(int argc, char* argv[]) {
    freopen("input.txt", "rt", stdin);
    scanf("%d", &TC);
    for (int T = 0; T < TC; T++) {
        scanf("%d %d", &W, &H);
        for (int h = 0; h < H; h++) {
            D[h] = 0;
            for (int w = 0; w < W; w++) {
                int tmp;
                scanf("%d", &tmp);
                if (tmp) {
                    D[h] |= (1 << w);
                    // ??
                    // vx
                    D[h] |= (1 << (w - 1));
                    if (h > 0) {
                        // vv
                        // ?x
                        D[h-1] |= (1 << (w - 1));
                        D[h-1] |= (1 << w);
                    }
                }
            }
            D[h] |= (1 << (W-1));
        }
        D[H-1] = (1 << (W + 1)) - 1;
        findAnswer(0, 0, 0);
        printf("#%d %d\n", T+1, Max);

        for (int h = 0; h < H; h++) {
            for (int w = 0; w < W; w++) {
                printf("%d ", (D[h] & (1 << w)) >> w);
            }
            printf("\n");
        }
        printf("\n");
    }

    return 0;
}
