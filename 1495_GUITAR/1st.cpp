// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define MAX_M 1001
#define MAX_N 101
#define INIT (-2)
#define min(a,b) ((a<b)?a:b)
#define max(a,b) ((a<b)?b:a)
#define abs(a) (((a)<0)?(-(a)):(a))

int T, N, S, M;
int t;
int V[MAX_N];
int Cache[MAX_M][MAX_N];

int maxVol(int s, int n) {
    if (s < 0 || M < s) return -1;
    if (n == N) return s;

    int &ret = Cache[s][n];
    if (ret != INIT) return ret;

    ret = max(ret, maxVol(s + V[n], n+1));
    ret = max(ret, maxVol(s - V[n], n+1));

    return ret;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    // scanf("%d", &T);
    T = 1;

    for (t = 0; t < T; t++) {
        scanf("%d %d %d", &N, &S, &M);

        for (int i = 0; i < N; i++) {
            scanf("%d", &V[i]);
            for (int j = 0; j < MAX_M; j++)
                Cache[j][i] = INIT;
        }

        printf("%d\n", maxVol(S, 0));
    }

    return 0;
}
