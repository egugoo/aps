// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

int T, N, M;
int t;

#define MAX_N 20
#define max(a,b) (a>b?a:b)

int price[MAX_N];
int like[MAX_N];
int C[201];

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d %d", &N, &M);
        printf("%d %d\n", N, M);

        M /= 100;

        for (int i = 0; i < N; ++i) {
            scanf("%d %d", &price[i], &like[i]);
            price[i] /= 100;

        }

        int answer = 0;
        for (int b = 1; b <= M; ++b) {
            int pick = 0;
            for (int menu = 0; menu < N; ++menu) {
                if (b >= price[menu]) {
                    pick = max(pick, C[(b - price[menu]) % 201] + like[menu]);
                }
            }
            C[b % 201] = pick;
            answer = max(answer, C[b%201]);
        }

        printf("%d\n", answer);

    }

    return 0;
}
