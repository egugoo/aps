// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

int T, N;
int t;


int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d", &N);
        printf("%d", N);
    }

    return 0;
}
