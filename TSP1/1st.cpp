// TSP1
// 1st

#include <stdio.h>

#define MAX_N 8

int C, N = 0;
double matrix[MAX_N][MAX_N] = { 0.0, };

int stack[MAX_N] = { 0, };
int cur = 0;

void push(int x) {
    stack[cur++] = x;
}

int pop() {
    return stack[--cur];
}

bool isEmpty() {
    return cur == 0;
}

double getTSP(bool picked[], int remain, int from, double sum) {
    if (remain == 0) return sum;

    double min = 987654321.0;

    for (int i = 0; i < N; i++) {
        if (picked[i]) continue;
        picked[i] = true;
        double tsp = getTSP(picked, remain - 1, i, sum + matrix[from][i]);
        if (tsp < min) min = tsp;
        picked[i] = false;
    }

    return min;
}

int main() {

    freopen("input.txt", "rt", stdin);

    scanf("%d", &C);

    for (int c = 0; c < C; c++) {

        scanf("%d", &N);

        double min = 987654321.0;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                scanf("%lf", &matrix[i][j]);
            }
        }

        double tsp = 987654321.0;

        for (int i = 0; i < N; i++) {
            int remain = N - 1;
            bool picked[MAX_N] = { false, };
            picked[i] = true;

            double sum = getTSP(picked, remain, i, 0.0);

            if (sum < tsp) tsp = sum;
        }

        printf("%.10lf\n", tsp);
    }

    return 0;
}
