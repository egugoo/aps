// TSP1
// 2nd

#include <iostream>
#include <stdio.h>


#define MAX_C 50
#define MAX_N 8

#define min(a,b) ((a<b)?a:b)


int T, N;
int t;
double D[MAX_N][MAX_N];
double DP[MAX_C][MAX_N][1<<MAX_N];
int BIT[MAX_N+1];


double tsp(int from, int flag) {
    if (flag == BIT[N] - 1) return 0.0;

    double &ret = DP[t][from][flag];
    if (ret) return ret;

    ret = 987654321.0;
    for (int i = 0; i < N; i++) {
        if (!(flag & BIT[i])) {
            ret = min(ret, D[from][i] + tsp(i, flag | BIT[i]));
        }
    }

    return ret;
}


int main() {
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (int i = 0; i <= MAX_N; i++) {
        BIT[i] = (1 << i);
    }

    for (t = 0; t < T; t++) {
        scanf("%d", &N);

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                scanf("%lf", &D[i][j]);
            }
        }

        double answer = 987654321.0;

        for (int i = 0; i < N; i++) {
            answer = min(answer, tsp(i, BIT[i]));
        }

        printf("%.10lf\n", answer);

    }

    return 0;
}
