// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define MAX_N 100
#define max(a, b) ((a<b)?b:a)

int T, N;
int t;
int Data[MAX_N];
long long Cache[MAX_N];

long long fib_rec(long long n) {
    if (2 <= n && !Cache[n])
        Cache[n] = fib_rec(n-1) + fib_rec(n-2);

    return Cache[n];
}

long long fib_for(long long n) {
    Cache[0] = 0;
    Cache[1] = 1;
    for (int i = 2; i <= n; ++i) {
        Cache[n] = Cache[i-1] + Cache[i-2];
    }

    return Cache[n];
}

int main()
{
    Cache[0] = 0;
    Cache[1] = 1;
    printf("%lld", fib_for(10));

    // freopen("input.txt", "rt", stdin);

    // scanf("%d", &T);

    // for (t = 0; t < T; t++) {
    //     scanf("%d", &N);
    //     for (int i = 0; i < N; i++) {
    //         scanf("%d", &Data[i]);
    //     }
    //     printf("%d\n", maxFallen());
    // }

    return 0;
}
