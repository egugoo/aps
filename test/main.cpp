// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

int T, N;
int t;

int A[1000];
int B[10] = {4,7,2,3,5};

#define max(a,b) ((a<b)?b:a)
#define abs(a) ((a>=0)?a:-a)

void pick(int picked, int toPick) {
    if (toPick == 0) {
        for (int i=0; i<picked; i++)
            printf("%d", A[i]);
        printf(" ");
        for (int i=0; i<picked; i++)
            printf("%d", B[A[i]]);
        printf("\n");
    }
    int smallest = (picked == 0) ? 0 : A[picked-1] + 1;
    for (int next = smallest; next < N; ++next) {
        A[picked++] = next;
        pick(picked--, toPick-1);
    }
}

int countBit(int n) {
    int i;
    for (i = 0; n != 0; ++i) {
        n &= (n-1);
    }
    return i;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d", &N);
        printf("%d", N);
    }

    N = 5;

    // for (int i=0; i < N; i++)
    //     for (int j=i+1; j < N; j++)
    //         for (int k=j+1; k < N; k++)
    //             for (int l=k+1; l < N; l++)
    //                 printf("(%d,%d,%d,%d)", i,j,k,l);

    A[0] = -1;
    pick(0, 4);

    for(int i = 0; i < (1<<N)-1; ++i) {
        if (countBit(i) == 4)
            printf("%02X %d\n", i, i-16);
    }

    return 0;
}
