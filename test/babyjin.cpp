// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define max(a, b) ((a<b)?b:a)

int T, N;
int t;
int Data[6];
int Counts[12] = {0,};

int main()
{
    freopen("input.txt", "rt", stdin);

    N = 6;

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        // scanf("%d", &N);
        for (int i = 0; i < 12; i++) {
            Counts[i] = 0;
        }
        for (int i = 0; i < N; i++) {
            scanf("%d", &Data[i]);
            Counts[Data[i]]++;
        }

        int tri = 0;
        int run = 0;
        int loop = 10;
        while (loop--) {
            if (Counts[loop] >= 3) {
                Counts[loop] -= 3;
                tri++;
                loop++;
                continue;
            }
            if (loop > 1 &&
                Counts[loop] >=1 &&
                Counts[loop-1] >= 1 &&
                Counts[loop-2] >= 1) {

                Counts[loop] -= 1;
                Counts[loop-1] -= 1;
                Counts[loop-2] -= 1;
                run++;
                loop++;
            }
        }

        if (run+tri == 2) {
            printf("Baby Gin\n");
        } else {
            printf("Lose\n");
        }
    }

    return 0;
}
