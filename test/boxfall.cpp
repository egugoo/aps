// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define MAX_N 100
#define max(a, b) ((a<b)?b:a)

int T, N;
int t;
int Data[MAX_N];

int maxFallen() {
    int ret = 0;
    for (int i = 0; i < N; i++) {
        int fall = N - i - 1;
        for (int j = i + 1; j < N; j++) {
            if (Data[i] <= Data[j]) {
                fall--;
            }
        }
        ret = max(ret, fall);
    }
    return ret;
}

int main()
{
    freopen("input.txt", "rt", stdin);

    scanf("%d", &T);

    for (t = 0; t < T; t++) {
        scanf("%d", &N);
        for (int i = 0; i < N; i++) {
            scanf("%d", &Data[i]);
        }
        printf("%d\n", maxFallen());
    }

    return 0;
}
