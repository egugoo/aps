#include <stdio.h>

void bitPrint(char n) {
    for(int i = 7; i >= 0; --i)
        putchar( (n&(1<<i)) ? '1': '0' );
    putchar( ' ' );
}
void ex1() {
    char i;
    for (i=-5; i<6; ++i) {
        printf("%3d: ", i);
        bitPrint(i);
        putchar('\n');
    }
}

void ex2() {
    // endianness
    char *p;
    char a = 0x10;
    int x = 0x01020304, i;

    printf("%d: ", a);
    p = &a;
    bitPrint(*p);
    putchar('\n');

    printf("0%X: ", x);
    p = (char *) &x;
    for (i = 0; i < 4; ++i)
        bitPrint(*p++);

    putchar('\n');
}

void ex3() {
    // check endianness
}

int main(int argc, char* argv[]) {
    ex1();
    putchar('\n');
    ex2();
    putchar('\n');
    ex3();
    return 0;
}
