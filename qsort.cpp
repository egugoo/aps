#include <stdio.h>

#define N 3

int d[10] = {9,7,1,2,5,1,7,9,6,8};

void qsort(int lo, int hi) {
    int p = d[(lo + hi) / 2];
    int l = lo;
    int r = hi;
    while (l < r) {
        while (d[l] < p) l++;
        while (d[r] > p) r--;
        if (l <= r) {
            int t = d[l];
            d[l] = d[r];
            d[r] = t;
            l++;
            r--;
        }
    }
    if (lo < r) qsort(lo, r);
    if (l < hi) qsort(l, hi);
}

int main(int argc, char* argv[]) {
    qsort(0, N-1);
    for (int i = 0; i < N; i++) {
        printf("%d ", d[i]);
    }
    printf("\n");
    return 0;
}
