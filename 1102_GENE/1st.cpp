// template file

#include <iostream>
#include <stdio.h>

// cmd+b: build
// cmd+shift+b: run

#define min(a,b) ((a<b)?a:b)

#define MAX_N 17

int T, N, P;
int t;
int Data[MAX_N][MAX_N];
int Cache[(1<<17)];

int minCost(int state) {
    int cnt = 0;
    for (int i = 0; state >> i; i++)
        if ((state >> i) & 1) cnt++;

    int ret = Cache[state];
    if (cnt >= P) ret = 0;
    if (ret != 987654321) return ret;

    for (int i = 0; i < N; i++) {
        if (!((state >> i) & 1)) continue;  // insure on i
        for (int j = 0; j < N; j++) {
            if (i == j) continue;
            if ((state >> j) & 1) continue;  // insure off j
            ret = min(ret, Data[i][j] + minCost(state | (1 << j)) );
        }
    }

    return ret;
}

int main()
{
    // freopen("input.txt", "rt", stdin);

    // scanf("%d", &T);
    T = 1;

    for (t = 0; t < T; t++) {
        scanf("%d", &N);

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                scanf("%d", &Data[i][j]);
        char a;
        int state = 0;
        for (int i = 0 ; i < N; i++) {
            scanf(" \n%c", &a);
            if (a == 'Y') state |= (1<<i);
        }

        for (int i = 0; i < 1<<N; i++)
            Cache[i] = 987654321;

        scanf("%d", &P);
        int cost = minCost(state);
        if (cost == 987654321) cost = -1;
        printf("%d\n", cost);
    }

    return 0;
}
